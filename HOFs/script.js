var ul = document.getElementById('ul'); // Get the list where we will place our authors
var url = 'https://randomuser.me/api/?results=5'; // Get 10 random users
var result;

fetch(url)
    .then(resp => resp.json())
    .then(data => {
       result = data.results;

      var mapper = result.map(result => {return result.name.first.toUpperCase()});
      var sortList = result.sort((a, b) => {
          if(a.name.last < b.name.last) { return -1; }
          if(a.name.last > b.name.last) { return 1; }
          return 0;
      });

      console.log(mapper);

      var filterList = result.filter(result => {
          return result.dob.age > 40;
      });

      var reduceList = result.reduce((sum, result) => {
            return sum + result.name.last.length;

      }, 0);

      var forEachList = result.forEach(result => {
          rec(result);
          function rec(result){
              for (var key in result) {
                  if (typeof result[key] === "object") {
                      rec(result[key]);
                  } else {
                      var li = document.createElement("li");
                      li.innerHTML = result[key];
                      ul.append(li);
                  }
              }
          }
          });
       console.log(result);
    });

