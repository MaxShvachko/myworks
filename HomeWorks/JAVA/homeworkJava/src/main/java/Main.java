import java.util.Arrays;

public class Main {

    public static void main(String[] args)
    {
        Conditions conditions = new Conditions();

//        int evenOdd = conditions.evenOdd(2, 5);
//        System.out.println(evenOdd);
//
//        String quater = conditions.quater(-5, -9);
//        System.out.println(quater);
//
//        int sumOfEven = conditions.sumOfPositive(-5, 1, 9);
//        System.out.println(sumOfEven);
//
//        int maxValue = conditions.maxValue(-5, 1, 9);
//        System.out.println(maxValue);
//
//        String studentGrade = conditions.studentGrade(25);
//        System.out.println(studentGrade);15
        Cycles cycles = new Cycles();
//        System.out.println(cycles.convertToPositiveNumber(-5));
//
//        int[] getSumEven = cycles.getSumEven();
//        System.out.println(Arrays.toString(getSumEven));
//
//        boolean getSimpleNumber = cycles.getSimpleNumber(5);
//        System.out.println(getSimpleNumber);
//
//        int getSquare = cycles.getSquare(15);
//        System.out.println(getSquare);
//
//        int getSquareBin = cycles.getSquareBin(26);
//        System.out.println(getSquareBin);
//
//        int getFactorial = cycles.getFactorial(5);
//        System.out.println(getFactorial);
//
//        int getFactorRec = cycles.getFactorRec(6);
//        System.out.println(getFactorRec);
//
//        int getSumOfNumber = cycles.getSumOfNumber(246);
//        System.out.println(getSumOfNumber);
//
//        int getMirrorNumber = cycles.getMirrorNumber(4321);
//        System.out.println(getMirrorNumber);

        Array arrays = new Array();

//        int getArrayMinElement = arrays.getArrayMinElement(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
//        System.out.println(getArrayMinElement);
//
//        int getArrayMaxElement = arrays.getArrayMaxElement(new int[] {1, 2, 3, 4, 25, 5, 6, 7, 8, 9});
//        System.out.println(getArrayMaxElement);
//
//        int getMinElementIndex = arrays.getMinElementIndex(new int[] {1, 2, 3, 4, 25, 5, 6, 7, 8, 9});
//        System.out.println(getMinElementIndex);
//
//        int getMaxElementIndex = arrays.getMaxElementIndex(new int[] {1, 2, 3, 4, 25, 5, 6, 7, 8, 9});
//        System.out.println(getMaxElementIndex);
//
//        int getSumOfOddArray =arrays.getSumOfOddArray(new int[] {1, 2, 3, 4, 25, 5, 6, 7, 8, 9});
//        System.out.println(getSumOfOddArray);
//
//        int[] getMirrorArray = arrays.getMirrorArray(new int[] {1, 2, 3, 4, 25, 5, 6, 7, 8, 9});
//        System.out.println(Arrays.toString(getMirrorArray));
//
//        int getQuantityOdd = arrays.getQuantityOdd(new int[] {1, 2, 3, 4, 25, 5, 6, 7, 8, 9});
//        System.out.println(getQuantityOdd);
//
//        int[] getHalfReverseArray = arrays.getHalfReverseArray(new int[] {1, 11, 7, 4, 25, 5, 6, 2, 8, 9});
//        System.out.println(Arrays.toString(getHalfReverseArray));
//
//        int[] getBubbleSort = arrays.getBubbleSort(new int[] {1, 11, 7, 4, 25, 5, 6, 2, 8, 9});
//        System.out.println(Arrays.toString(getBubbleSort));
//
//        int[] selectSort = arrays.selectSort(new int[] {1, 11, 7, 4, 25, 5, 6, 2, 8, 9});
//        System.out.println(Arrays.toString(selectSort));
//
//        int[] insertSort = arrays.insertSort(new int[] {1, 11, 7, 4, 25, 5, 6, 2, 8, 9});
//        System.out.println(Arrays.toString(insertSort));

        Functions func = new Functions();

//        String dayNameByNumber = func.dayNameByNumber(5);
//        System.out.println(dayNameByNumber);

//        float getDecardLong = func.getDecardLong(5, 4, 5, 2);
//        System.out.println(getDecardLong);
//
        int actual = 125;
        String convertNumberIntoWord = Functions.convertNumberIntoWord(actual);
        System.out.println(convertNumberIntoWord);

//        String expected = "nine hundred ninety nine";
//        int convertWordIntoNumber = Functions.convertWordIntoNumber(expected);
//        System.out.println(convertWordIntoNumber);
    }
}