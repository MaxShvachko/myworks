public class Array {
    public static int getArrayMinElement(int[] newArray) {

        int result = newArray[0];
        for (int i = 0; i < newArray.length; i++) {
            if (result > newArray[i]) {
                result = newArray[i];
            }
        }
        return result;
    }

    public static int getArrayMaxElement(int[] newArray) {
        int result = newArray[0];

        for (int i = 0; i < newArray.length; i++) {
            if (result < newArray[i]) {
                result = newArray[i];
            }
        }
        return result;
    }

    public static int getMinElementIndex(int[] newArray) {
        int index = 0;

        for (int i = 1; i < newArray.length; i++) {
            if (newArray[index] > newArray[i]) {
                index = i;
            }
        }
        return index;
    }

    public static int getMaxElementIndex(int[] newArray) {
        int result = newArray[0];
        int index = 0;

        for (int i = 0; i < newArray.length; i++) {
            if (result < newArray[i]) {
                index = i;
            }
        }
        return index;
    }

    public static int getSumOfOddArray(int[] newArray) {
        int sum = 0;

        for (int i = 0; i < newArray.length; i += 2) {
            sum += newArray[i];
        }
        return sum;
    }

    public static int[] getMirrorArray(int[] newArray) {
        int index;
        index = 0;
        int[] mirror = new int[newArray.length];
        int lengthArray = newArray.length;
        for (int i = lengthArray - 1; i >= 0; i--) {
            mirror[index] = newArray[i];
            index++;
        }
        return mirror;
    }

    public static int getQuantityOdd(int[] newArray) {
        var count = 0;

        for (int i = 0; i < newArray.length; i++) {
            if (newArray[i] % 2 != 0) {
                count++;
            }
        }
        return count;
    }

    public static int[] getHalfReverseArray(int[] newArray) {
        int half = newArray.length / 2;
        int div = half + newArray.length % 2;
        for (int i = 0; i < div; i++)
        {
            int currentElement = newArray[i];
            newArray[i] = newArray[div + i];
            newArray[div + i] = currentElement;
        }
        return newArray;
        }

    public static int[] getBubbleSort(int[] array) {

        int lengthCircle = array.length - 1;

        for (int i = 0; i < lengthCircle; i++) {
            for (int j = 0; j < lengthCircle; j++) {
                if (array[j] > array[j +1]) {
                    int arrayValue = array[j];
                    array[j] =  array[j +1];
                    array[j + 1] = arrayValue;
                }
            }
        }
        return array;
    }

    public static int[] selectSort(int[] array) {

        for(int i = 0; i < array.length; i++){
            int minElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minElement]) {
                    minElement = j;
                }
                if (minElement != i) {
                    int temp = array[i];
                    array[i] = array[minElement];
                    array[minElement] = temp;
                }
            }
        }
        return array;
    }

    public static int[] insertSort(int[] array) {

        int length = array.length;
        for (int i = 1; i < length; i++) {
            int key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
        return array;
    };

}
