public class Functions {
    public static String dayNameByNumber(int dayNumber) {
        String error;
        if (dayNumber <= 0 || dayNumber >= 8) {
            error = "Enter number in range 1 - 7";
            return error;
        }

        String[] week = {
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday",
                "Sunday"
        };
        return week[dayNumber - 1];
    }

    public static float getDecardLong(int x1, int x2, int y1, int y2) {
        return (float) Math.sqrt((Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2)));
    }

    public static String convertNumberIntoWord(int number) {
        int temp = number;
        String result = "";
        String[] unitText = {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine",
                "Ten"
        };
        String[] unitTen = {
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
        };
        String[] tensText = {
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
        };

        if(temp <= 10) {
            result = unitText[temp];
        }else if (temp > 10 && temp < 20) {
            result = unitTen[temp - 11];
        }else if (temp >= 20 && temp < 100) {
            if (temp % 10 == 0) {
                result = tensText[(int) (Math.floor(temp / 10) - 2)];
            }else {
                result = tensText[(int) (Math.floor(temp / 10) - 2)] + " " + unitText[temp % 10];
            }
        }else if (temp >= 100 && temp < 1000) {
            result = unitText[(int) Math.floor(temp / 100)] + " hundred";
            if (temp % 100 == 0) {
                result = result;
            }else if (temp % 100 <= 10) {
                result += " " + unitText[temp % 100];
            }else if (temp % 100 > 10 && temp % 100 < 20) {
                result += " " + unitTen[temp % 100 - 11];
            }else if (temp % 100 >= 20 && temp % 100 < 100) {
                if (temp % 10 == 0) {
                    result += " " + tensText[(int) (Math.floor((temp % 100) / 10) - 2)];
                }
                else{
                    result += " " + tensText[(int) (Math.floor((temp % 100) / 10) - 2)] + " " + unitText[temp % 10];
                }
            }
        }
        return result;
    }


    public static int convertWordIntoNumber(String word) {
        int numberOfWord;
        numberOfWord = 0;
        String temp = word.toLowerCase();
        String[] tempList;
        String[] unitText = {
                "zero",
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine",
                "ten"
        };
        String[] unitTen = {
                "eleven",
                "twelve",
                "thirteen",
                "fourteen",
                "fifteen",
                "sixteen",
                "seventeen",
                "eighteen",
                "nineteen",
                "",
                ""
        };
        String[] tensText = {
                "twenty",
                "thirty",
                "forty",
                "fifty",
                "sixty",
                "seventy",
                "eighty",
                "ninety",
                "",
                "",
                "",
        };
        tempList = temp.split(" ");
        int Length = unitText.length;

        for (int i = 0; i < Length; i++) {
            if (tempList.length == 1) {
                if (tempList[0].equals(unitText[i])) {
                    numberOfWord += i;
                } else if (tempList[0].equals(unitTen[i])) {
                    numberOfWord += i + 11;
                } else if (tempList[0].equals(tensText[i])) {
                    numberOfWord += (i + 2) * 10;
                }
            } else if (tempList.length == 2) {
                if (tempList[1].equals("hundred")) {
                    if (tempList[0].equals(unitText[i])) {
                        numberOfWord += i * 100;
                    }
                } else if (tempList[0].equals(tensText[i])) {
                    numberOfWord += ((i + 2) * 10);
                }else if (tempList[1].equals(unitText[i])) {
                    numberOfWord += i;
                }
            } else if (tempList.length == 3) {
                if (tempList[0].equals(unitText[i])) {
                    numberOfWord += i * 100;
                }
                if (tempList[2].equals(unitText[i])) {
                    numberOfWord += i;
                }
                if (tempList[2].equals(unitTen[i])) {
                    numberOfWord += i + 11;
                }
                if (tempList[2].equals(tensText[i])) {
                    numberOfWord += (i + 2) * 10;
                }
            } else{
                if (tempList[0].equals(unitText[i])) {
                    numberOfWord += i * 100;
                } else if (tempList[2].equals(tensText[i])) {
                    numberOfWord += (i + 2) * 10;
                }
                if (tempList[3].equals(unitText[i])) {
                    numberOfWord += i;
                }
            }
        }
        return numberOfWord;
    }
}

