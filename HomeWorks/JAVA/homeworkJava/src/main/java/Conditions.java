public class Conditions {
    public static int evenOdd(int a, int b) {
        int result;
        if (a % 2 == 0) {
            result = a * b;
        }else {
            result = a + b;
        }
        return result;
    }

    public static String quater(int x, int y) {
        String  message;

        if (x > 0 && y > 0) {
            message = "quarter I";
        } else if (x < 0 && y > 0) {
            message = "quarter II";
        } else if (x < 0 && y < 0) {
            message = "quarter III";
        } else if (x > 0 && y < 0) {
            message = "quarter IV";
        } else if (x == 0 && y == 0) {
            message = "start of coordinates";
        }

        else if (x == 0 && y > 0) {
            message = "X on axis x. Point between quarter I and quater II";
        } else if (y < 0) {
            message = "X on axis x. Point between quarter III and quater IV";
        } else if (0 < x) {
            message = "Y on axis y. Point between quarter I and quater IV";
        } else {
            message = "Y on axis y. Point between quarter II and quater III";
        }
        return message;
    }

    public static int sumOfPositive(int a, int b, int c) {
        int sum = 0;

        if (a > 0) {
            sum += a;
        }
        if (b > 0) {
            sum += b;
        }
        if (c > 0){
            sum += c;
        }
        return sum;
    }

    public static int maxValue(int a, int b, int c) {
        int maxFirst = a * b * c + 3;
        int maxSecond = a + b + c + 3;

        if (maxFirst > maxSecond) {
            return maxFirst;
        }else {
            return maxSecond;
        }
    }

    public static String studentGrade(int grade) {
        String score = "";

        if (grade < 0 || grade > 100) {
            return ("Rating should be in the range from 0 to 100 points");
        }

        if (grade <= 19) {
            score = "Grade F";
        } else if (grade <= 39) {
            score = "Grade E";
        } else if (grade <= 59) {
            score = "Grade D";
        } else if (grade <= 74) {
            score = "Grade C";
        } else if (grade <= 89) {
            score = "Grade B";
        } else {
            score = "Grade A";
        }
        return score;
    }
}
