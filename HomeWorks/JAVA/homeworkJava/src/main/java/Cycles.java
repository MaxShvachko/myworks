public class Cycles {
    public static int convertToPositiveNumber(int number) {
        if (number < 0) {
            number = -number;
        }
        return number;
    }

    public static int[] getSumEven() {
        int counter = 0;
        int sum = 0;
        for (int i = 2; i < 99; i++) {
            if (i % 2 == 0) {
                sum += i;
                counter++;
            }
        }
//        int[]array = {sum, counter};
        return new int[] {sum, counter};
    }

    public static boolean getSimpleNumber(int number) {
        convertToPositiveNumber(number);
        boolean mark = true;

        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                mark = false;
                break;
            }
        }
        return mark;
    }

    public static int getSquare(int n) {
        if (n < 0) {
            n = -n;
        }
        int squareN = n;

        while (squareN * squareN > n) {
            squareN--;
        }
        return squareN;
    }

    public static int getSquareBin(int n) {
        if (n < 0) {
            n = -n;
        }

        int squareN = n;
        while (squareN * squareN > n) {
            squareN /= 2;
        }
        while (squareN * squareN < n) {
            squareN++;
            if (squareN * squareN > n) {
                squareN -= 1;
                break;
            }
        }
        return squareN;
    }

    public static int getFactorial(int n) {
        if (n < 0) {
            n = -n;
        }

       int factor = 1;

        for (int i = 1; i <= n; i++) {

            factor *= i;
        }
        return factor;
    }

    public static int getFactorRec(int n) {
        if (n < 0) {
            n = -n;
        }

        if (n == 0 || n == 1) {
            return 1;
        }
        return n * getFactorRec(n - 1);
    }

    public static int getSumOfNumber(int n) {
        if (n < 0) {
            n = -n;
        }

        int temp = n;
        int remainder = 0;
        int result = 0;

        while (temp > 0) {
            remainder = temp % 10;
            result += remainder;
            temp = (temp - remainder) / 10;
        }
        return result;
    }

    public static int getMirrorNumber(int n) {
        if (n < 0) {
            n = -n;
        }

        int temp = n;
        int remainder = 0;
        int result = 0;

        while (temp > 0) {
            remainder = temp % 10;
            result += remainder * 10;
            result *= 10;
            temp = (temp - remainder) / 10;
        }
        result /= 100;
        return result;
    }
}
