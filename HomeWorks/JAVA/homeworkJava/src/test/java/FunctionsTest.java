import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FunctionsTest {

    @Test
    void dayNameByNumber() {
        String action = Functions.dayNameByNumber(5);
        String expected = "Friday";
        assertEquals(expected, action);
    }
    @Test
    void dayNameByNumberError() {
        String action = Functions.dayNameByNumber(-2);
        String expected = "Enter number in range 1 - 7";
        assertEquals(expected, action);
    }


    @Test
    void getDecardLong() {
        float action = Functions.getDecardLong(5, 4, 5, 2);
        float expected = (float) 3.1622777;
        assertEquals(expected, action);
    }

    @Test
    void convertNumberIntoWord125() {
        String action = Functions.convertNumberIntoWord(125);
        String expected = "One hundred Twenty Five";
        assertEquals(expected, action);
    }
    @Test
    void convertNumberIntoWord25() {
        String action = Functions.convertNumberIntoWord(25);
        String expected = "Twenty Five";
        assertEquals(expected, action);
    }
    @Test
    void convertNumberIntoWord5() {
        String action = Functions.convertNumberIntoWord(5);
        String expected = "Five";
        assertEquals(expected, action);
    }
    @Test
    void convertNumberIntoWord500() {
        String action = Functions.convertNumberIntoWord(15);
        String expected = "Fifteen";
        assertEquals(expected, action);
    }
    @Test
    void convertNumberIntoWord15() {
        String action = Functions.convertNumberIntoWord(15);
        String expected = "Fifteen";
        assertEquals(expected, action);
    }

    @Test
    void convertWordIntoNumber() {
        int action = Functions.convertWordIntoNumber("One hundred Twenty Five");
        int expected = 125;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber100() {
        int action = Functions.convertWordIntoNumber("One hundred");
        int expected = 100;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber40() {
        int action = Functions.convertWordIntoNumber("Forty");
        int expected = 40;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber999() {
        int action = Functions.convertWordIntoNumber("Nine hundred Ninety Nine");
        int expected = 999;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber16() {
        int action = Functions.convertWordIntoNumber("sixteen");
        int expected = 16;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber35() {
        int action = Functions.convertWordIntoNumber("thirty five");
        int expected = 35;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber82() {
        int action = Functions.convertWordIntoNumber("eighty two");
        int expected = 82;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber79() {
        int action = Functions.convertWordIntoNumber("seventy nine");
        int expected = 79;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber416() {
        int action = Functions.convertWordIntoNumber("four hundred sixteen");
        int expected = 416;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber21() {
        int action = Functions.convertWordIntoNumber("twenty one");
        int expected = 21;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber31() {
        int action = Functions.convertWordIntoNumber("thirty one");
        int expected = 31;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber65() {
        int action = Functions.convertWordIntoNumber("sixty five");
        int expected = 65;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber66() {
        int action = Functions.convertWordIntoNumber("sixty six");
        int expected = 66;
        assertEquals(expected, action);
    }
    @Test
    void convertWordIntoNumber83() {
        int action = Functions.convertWordIntoNumber("eighty three");
        int expected = 83;
        assertEquals(expected, action);
    }







}