import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTest {

    @Test
    void getArrayMinElement2() {
        int action = Array.getArrayMinElement(new int[]{9, 8, 6, 7, 2, 6, 9});
        int expected = 2;
        assertEquals(expected, action);
    }
    @Test
    void getArrayMinElement6() {
        int action = Array.getArrayMinElement(new int[]{10, 8, 6, 7, 252, 6, 9});
        int expected = 6;
        assertEquals(expected, action);
    }

    @Test
    void getArrayMaxElement9() {
        int action = Array.getArrayMaxElement(new int[]{9, 8, 6, 7, 2, 6, 9});
        int expected = 9;
        assertEquals(expected, action);
    }
    @Test
    void getArrayMaxElement256() {
        int action = Array.getArrayMaxElement(new int[]{10, 8, 6, 7, 252, 6, 9});
        int expected = 252;
        assertEquals(expected, action);
    }


    @Test
    void getMinElementIndex4() {
        int action = Array.getMinElementIndex(new int[]{9, 8, 6, 7, 2, 6, 9});
        int expected = 4;
        assertEquals(expected, action);
    }

    @Test
    void getMinElementIndex2() {
        int action = Array.getMinElementIndex(new int[]{10, 8, 6, 7, 252, 6, 10});
        int expected = 2;
        assertEquals(expected, action);
    }

    @Test
    void getMaxElementIndex0() {
        int action = Array.getMaxElementIndex(new int[]{ 9, 8, 6, 7, 2, 6, 9});
        int expected = 0;
        assertEquals(expected, action);
    }
    @Test
    void getMaxElementIndex4() {
        int action = Array.getMaxElementIndex(new int[]{10, 8, 6, 7, 252, 6, 10});
        int expected = 4;
        assertEquals(expected, action);
    }

    @Test
    void getSumOfOddArray() {
        int action = Array.getSumOfOddArray(new int[]{9, 8, 6, 7, 2, 6, 9});
        int expected = 26;
        assertEquals(expected, action);
    }
    @Test
    void getSumOfOddArray278() {
        int action = Array.getSumOfOddArray(new int[]{10, 8, 6, 7, 252, 6, 10});
        int expected = 278;
        assertEquals(expected, action);
    }

    @Test
    void getMirrorArray1() {
        int[] action = Array.getMirrorArray(new int[] { 9, 8, 6, 7, 2, 6, 9});
        int[] expected = {9, 6, 2, 7, 6, 8, 9};
        assertArrayEquals(expected, action);
    }
    @Test
    void getMirrorArray2() {
        int[] action = Array.getMirrorArray(new int[] {10, 8, 6, 7, 252, 6, 10});
        int[] expected = {10, 6, 252, 7, 6, 8, 10};
        assertArrayEquals(expected, action);
    }

    @Test
    void getQuantityOdd3() {
        int action = Array.getQuantityOdd(new int[] {9, 8, 6, 7, 2, 6, 9});
        int expected = 3;
        assertEquals(expected, action);
    }
    @Test
    void getQuantityOdd1() {
        int action = Array.getQuantityOdd(new int[] {10, 8, 6, 7, 252, 6, 10});
        int expected = 1;
        assertEquals(expected, action);
    }

    @Test
    void getHalfReverseArray() {
        int[] action = Array.getHalfReverseArray(new int[] {10, 8, 6, 7, 252, 6, 10, 8});
        int[] expected = {252, 6, 10, 8, 10, 8, 6, 7};
        assertArrayEquals(expected, action);
    }
    @Test
    void getHalfReverseArray2() {
        int[] action = Array.getHalfReverseArray(new int[] {2, 3, 4, 25, 6, 7, 51, 17});
        int[] expected = {6, 7, 51, 17, 2, 3, 4, 25};
        assertArrayEquals(expected, action);
    }

    @Test
    void getBubbleSort() {
        int[] action = Array.getBubbleSort(new int[] {10, 8, 6, 7, 252, 6, 10, 8});
        int[] expected = {6, 6, 7, 8, 8, 10, 10, 252};
        assertArrayEquals(expected, action);
    }

    @Test
    void selectSort() {
        int[] action = Array.selectSort(new int[] {10, 8, 6, 7, 252, 6, 10, 8});
        int[] expected = {6, 6, 7, 8, 8, 10, 10, 252};
        assertArrayEquals(expected, action);

    }

    @Test
    void insertSort() {
        int[] action = Array.insertSort(new int[] {10, 8, 6, 7, 252, 6, 10, 8});
        int[] expected = {6, 6, 7, 8, 8, 10, 10, 252};
        assertArrayEquals(expected, action);
    }
}