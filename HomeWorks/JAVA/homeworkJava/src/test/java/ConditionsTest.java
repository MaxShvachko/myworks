import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConditionsTest {

    @org.junit.jupiter.api.Test

    void evenOddisEven() {
        int action = Conditions.evenOdd(2, 5);
        int expected = 10;
        assertEquals(expected, action);
    }
    @Test
    void evenOddisOdd() {
        int action = Conditions.evenOdd(3, 5);
        int expected = 8;
        assertEquals(expected, action);
    }

    @org.junit.jupiter.api.Test
    void quaterStartofCorordinate() {
        String action = Conditions.quater(0, 0);
        String expected = "start of coordinates";
        assertEquals(expected, action);
    }
    @Test
    void quaterI() {
        String action = Conditions.quater(2, 9);
        String expected = "quarter I";
        assertEquals(expected, action);
    }

    @Test
    void quaterII() {
        String action = Conditions.quater(-2, 9);
        String expected = "quarter II";
        assertEquals(expected, action);
    }
    @Test
    void quaterIII() {
        String action = Conditions.quater(-2, -9);
        String expected = "quarter III";
        assertEquals(expected, action);
    }
    @Test
    void quaterIV() {
        String action = Conditions.quater(2, -9);
        String expected = "quarter IV";
        assertEquals(expected, action);
    }
    @Test
    void quaterOnXBetweenIII_IV() {
        String action = Conditions.quater(0, -9);
        String expected = "X on axis x. Point between quarter III and quater IV";
        assertEquals(expected, action);
    }
    @Test
    void quaterOnXBetweenI_II() {
        String action = Conditions.quater(0, 9);
        String expected = "X on axis x. Point between quarter I and quater II";
        assertEquals(expected, action);
    }
    @Test
    void quaterOnYBetweenI_IV() {
        String action = Conditions.quater(2, 0);
        String expected = "Y on axis y. Point between quarter I and quater IV";
        assertEquals(expected, action);
    }
    @Test
    void quaterOnYBetweenII_III() {
        String action = Conditions.quater(-5, 0);
        String expected = "Y on axis y. Point between quarter II and quater III";
        assertEquals(expected, action);
    }


    @org.junit.jupiter.api.Test
    void sumOfEvenWhenAIsNegative() {
        int action = Conditions.sumOfPositive(-5, 9, 2);
        int expected = 11;
        assertEquals(expected, action);
    }

    @Test
    void sumOfEven() {
        int action = Conditions.sumOfPositive(5, 9, 2);
        int expected = 16;
        assertEquals(expected, action);
    }
    @Test
    void sumOfEvenBIsNegative() {
        int action = Conditions.sumOfPositive(5, -9, 2);
        int expected = 7;
        assertEquals(expected, action);
    }
    @Test
    void sumOfEvenCIsNegative() {
        int action = Conditions.sumOfPositive(5, 9, -2);
        int expected = 14;
        assertEquals(expected, action);
    }


    @org.junit.jupiter.api.Test
    void maxValue() {
        int action = Conditions.maxValue(5, 9, -2);
        int expected = 15;
        assertEquals(expected, action);

    }
    @Test
    void maxValue2() {
        int action = Conditions.maxValue(1, 9, 2);
        int expected = 21;
        assertEquals(expected, action);

    }

    @org.junit.jupiter.api.Test
    void studentGrade15() {
        String action = Conditions.studentGrade(15);
        String expected = "Grade F";
        assertEquals(expected, action);
    }
    @Test
    void studentGrade27() {
        String action = Conditions.studentGrade(27);
        String expected = "Grade E";
        assertEquals(expected, action);
    }
    @Test
    void studentGrade40() {
        String action = Conditions.studentGrade(40);
        String expected = "Grade D";
        assertEquals(expected, action);
    }
    @Test
    void studentGrade56() {
        String action = Conditions.studentGrade(65);
        String expected = "Grade C";
        assertEquals(expected, action);
    }
    @Test
    void studentGrade85() {
        String action = Conditions.studentGrade(85);
        String expected = "Grade B";
        assertEquals(expected, action);
    }
    @Test
    void studentGrade95() {
        String action = Conditions.studentGrade(95);
        String expected = "Grade A";
        assertEquals(expected, action);
    }
    @Test
    void studentGrade_15() {
        String action = Conditions.studentGrade(-15);
        String expected = "Rating should be in the range from 0 to 100 points";
        assertEquals(expected, action);
    }
    @Test
    void studentGrade_125() {
        String action = Conditions.studentGrade(125);
        String expected = "Rating should be in the range from 0 to 100 points";
        assertEquals(expected, action);
    }
}