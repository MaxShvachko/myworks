import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CyclesTest {

    @Test
    void convertToPositiveNumber() {
        int action = Cycles.convertToPositiveNumber(-5);
        int expected = 5;
        assertEquals(expected, action);
    }

    @Test
    void getSumEven() {
        int[] action = Cycles.getSumEven();
        int[] expected = {2450, 49};
        assertArrayEquals(expected, action);
    }

    @Test
    void getSimpleNumber() {
        boolean action = Cycles.getSimpleNumber(5);
        boolean expected = true;
        assertEquals(expected, action);
    }
    @Test
    void getSimpleNumberFalse() {
        boolean action = Cycles.getSimpleNumber(6);
        boolean expected = false;
        assertEquals(expected, action);
    }

    @Test
    void getSquare() {
        int action = Cycles.getSquare(11);
        int expected = 3;
        assertEquals(expected, action);
    }

    @Test
    void getSquareNegative() {
        int action = Cycles.getSquare(-12);
        int expected = 3;
        assertEquals(expected, action);
    }

    @Test
    void getSquareBin() {
        int action = Cycles.getSquareBin(26);
        int expected = 5;
        assertEquals(expected, action);
    }

    @Test
    void getFactorial() {
        int action = Cycles.getFactorial(5);
        int expected = 120;
        assertEquals(expected, action);

    }

    @Test
    void getFactorRec() {
        int action = Cycles.getFactorRec(5);
        int expected = 120;
        assertEquals(expected, action);
    }

    @Test
    void getSumOfNumber() {
        int action = Cycles.getSumOfNumber(261);
        int expected = 9;
        assertEquals(expected, action);
    }

    @Test
    void getMirrorNumber() {
        int action = Cycles.getMirrorNumber(261);
        int expected = 162;
        assertEquals(expected, action);
    }
}