function convertToPositiveNumber(number) {
    if (number < 0) {
        number = -number;
        return number;
    }
}

function getSumEven() {
    var counter = 0;
    var sum = 0;
    for (var i = 2; i < 99; i++) {
        if (i % 2 === 0) {
            sum += i;
            counter++;
        }
    }
    return `${sum}, ${counter}`;
}

function getSimpleNumber(number) {
    if (number < 0) {
        number = -number;
    }
    if (typeof number !== "number") {
        return -1;
    }
    var mark = true;

    for (var i = 2; i <= number / 2; i++) {
        if (number % i === 0) {
            mark = false;
            break;
        }
    }
    return mark;
}

function getSquare(n) {
    if (typeof n !== "number") {
        return -1;
    }
    if (n < 0) {
        (n = -n);
    }
    var squareN = n;

    while (squareN * squareN > n) {
        squareN--;
    }
    return squareN;
}

function getSquareBin(n) {
    if (n < 0) {
        (n = -n);
    }
    if (typeof n !== "number") {
        return -1;
    }
    var squareN = n;
    while (squareN * squareN > n) {
        (squareN /= 2);
    }
    while (squareN * squareN < n) {
        squareN++;
        if (squareN * squareN > n) {
            squareN -= 1;
            break;
        }
    }
    return ~~squareN;
}

function getFactorial(n) {
    if (n < 0) {
        n = -n;
    }
    if (typeof n !== "number") {
        return -1;
    }

    var factor = 1;

    for (var i = 1; i <= n; i++) {

        factor *= i;
    }
    return factor;
}

function getFactorRec(n) {
    if (n < 0) {
        n = -n;
    }
    if (typeof n !== "number") {
        return -1;
    }

    if (n === 0 || n === 1) {
        return 1;
    }
    return n * getFactorRec(n - 1);
}

function getSumOfNumber(n) {
    if (n < 0) {
        n = -n;
    }
    if (typeof n !== "number") {
        return -1;
    }

    var temp = n;
    var remainder = 0;
    var result = 0;

    while (temp > 0) {
        remainder = temp % 10;
        result += remainder;
        temp = (temp - remainder) / 10;
    }
    return result;
}

function getMirrorNumber(n) {
    if (n < 0) {
        n = -n;
    }
    if (typeof n !== "number") {
        return -1;
    }
    var temp = n;
    var remainder = 0;
    var result = 0;

    while (temp > 0) {
        remainder = temp % 10;
        result += remainder * 10;
        result *= 10;
        temp = (temp - remainder) / 10;
    }
    result /= 100;
    return result;
}

module.exports = {
    convertToPositiveNumber,
    getSumEven,
    getSimpleNumber,
    getSquare,
    getSquareBin,
    getFactorial,
    getFactorRec,
    getSumOfNumber,
    getMirrorNumber,
};

