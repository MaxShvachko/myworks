const assert  = require('chai').assert;
const app = require('../index');

getSumEven = app.getSumEven;
getSimpleNumber = app.getSimpleNumber;
getSquare = app.getSquare;
getSquareBin = app.getSquareBin;
getFactorial = app.getFactorial;
getFactorRec = app.getFactorRec;
getSumOfNumber = app.getSumOfNumber;
getMirrorNumber = app.getMirrorNumber;
convertToPositiveNumber = app.convertToPositiveNumber;



describe("convertToPositiveNumber", function () {
  it("argument is negative -25 convert to 25", function () {
    assert.equal(convertToPositiveNumber(-25), 25);
  });
});


describe("getSumEven sum of number in range 0 - 99 ", function () {
  it("sum of even and they quantity", function () {
    assert.equal(getSumEven(), "2450, 49");
  });
});

describe("getSimpleNumber ", function () {
  it("number 2 is simple", function () {
    assert.equal(getSimpleNumber(2), true);
  });
  it("number 643 is simple", function () {
    assert.equal(getSimpleNumber(643), true);
  });
  it("number 8819 is simple", function () {
    assert.equal(getSimpleNumber(8819), true);
  });
  it("number 8732 isn`t simple", function () {
    assert.equal(getSimpleNumber(8732), false);
  });
  it("number -8732 isn`t simple", function () {
    assert.equal(getSimpleNumber(-8732), false);
  });
  it("argument isn`t number - -1", function () {
    assert.equal(getSimpleNumber("Hello world"), -1);
  });
});

describe("getSquare ", function () {
  describe("sequential selection", function () {
    it("Square 75 = 8", function () {
      assert.equal(getSquare(75), 8);
    });
    it("Square 1539 = 39", function () {
      assert.equal(getSquare(1539), 39);
    });
    it("Square з -546 = 23", function () {
      assert.equal(getSquare(-546), 23);
    });
    it("Square 0 = 0", function () {
      assert.equal(getSquare(0), 0);
    });
    it("argument isn`t number - -1", function () {
      assert.equal(getSquare("Hello world"), -1);
    });
  });
  //метод бинарного поиска
  describe("binary search of square", function () {
    it("Square 48 = 6", function () {
      assert.equal(getSquareBin(48), 6);
    });
    it("Square 32 = 5", function () {
      assert.equal(getSquareBin(32), 5);
    });
    it("Square 1539 = 39", function () {
      assert.equal(getSquareBin(1539), 39);
    });
    it("Square 546 = 23", function () {
      assert.equal(getSquareBin(-546), 23);
    });
    it("Square 0 = 0", function () {
      assert.equal(getSquareBin(0), 0);
    });
    it("argument isn`t number - -1", function () {
      assert.equal(getSquareBin("Hello world"), -1);
    });
  });
});

describe("Factorial n", function() {
  it ("Factorial 5 = 120", function () {
    assert.equal(getFactorial(5), 120);
  });
  it ("Factorial 0 = 1", function () {
    assert.equal(getFactorial(0), 1);
  });
  it ("Factorial 12 = 479001600", function () {
    assert.equal(getFactorial(12), 479001600);
  });
  it ("Factorial 1 = 1", function () {
    assert.equal(getFactorial(1), 1);
  });
  it("argument isn`t number - -1", function () {
    assert.equal(getFactorial("Hello world"), -1);
  });
  it("argument is negative number - -5 convert to 5", function () {
    assert.equal(getFactorial(-5), 120);
  });
});

describe("Factorial n Recursion ", function() {
  it ("Factorialа 5 = 120", function () {
    assert.equal(getFactorRec(5), 120);
  });
  it ("Factorial 0 = 1", function () {
    assert.equal(getFactorRec(0), 1);
  })
  it ("Factorial 12 = 479001600", function () {
    assert.equal(getFactorRec(12), 479001600);
  })
  it ("Factorial 1 = 1", function () {
    assert.equal(getFactorRec(1), 1);
  });
  it("argument isn`t number - -1", function () {
    assert.equal(getFactorRec("Hello world"), -1);
  });
  it("argument is negative number - -5 convert to 5", function () {
    assert.equal(getFactorRec(-5), 120);
  });
});

describe("getSumOfNumber", function() {
  it ("Sum of numbers 577 = 19", function () {
    assert.equal(getSumOfNumber(577), 19);
  });
  it ("Sum of numbers 349 = 16", function () {
    assert.equal(getSumOfNumber(349), 16);
  })
  it ("Sum of numbers 5 = 5", function () {
    assert.equal(getSumOfNumber(5), 5);
  })
  it ("Sum of numbers 123456789 = 16Є", function () {
    assert.equal(getSumOfNumber(123456789), 45);
  });
  it("argument isn`t number - -1", function () {
    assert.equal(getSumOfNumber("Hello world"), -1);
  });
  it("argument is negative number - -5 convert to 5", function () {
    assert.equal(getSumOfNumber(-56), 11);
  });
});

describe("getMirrorNumber, reverse number", function() {
  it ("reverse number 578 = 875", function () {
    assert.equal(getMirrorNumber(578), 875);
  });
  it ("reverse number 654321 = 123456", function () {
    assert.equal(getMirrorNumber(654321), 123456);
  })
  it ("reverse number 5 = 5", function () {
    assert.equal(getMirrorNumber(5), 5);
  })
  it ("reverse number -12354 = 45321", function () {
    assert.equal(getMirrorNumber(-12354), 45321);
  });it("argument isn`t number - -1", function () {
    assert.equal(getMirrorNumber("Hello world"), -1);
  });
});
