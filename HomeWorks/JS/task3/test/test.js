var assert  = require('chai').assert;
var app = require('../homeWork_array');
var babelReg = require("@babel/register");

getArrayMinElement = app.getArrayMinElement;
getArrayMaxElement = app.getArrayMaxElement;
getMinElementIndex = app.getMinElementIndex;
getMaxElementIndex = app.getMaxElementIndex;
getSumOfOddArray = app.getSumOfOddArray;
getMirrorArray = app.getMirrorArray;
getQuantityOdd = app.getQuantityOdd;
getHalfReverseArray = app.getHalfReverseArray;
getBubbleSort = app.getBubbleSort;
selectSort = app.selectSort;
insertSort = app.insertSort;

var object = {name: 'Alex', lastname: 'Morgan'};
describe("arrayMinElement find minimal element of array", function () {

  it("in array [9, 8, 6, 7, 2, 6, 9] - minimal element - 2", function () {
    assert.equal(getArrayMinElement([9, 8, 6, 7, 2, 6, 9]), 2);
  });
  it("in array [10, 8, 6, 7,252, 6, 9] - minimal element - 6", function () {
    assert.equal(getArrayMinElement([10, 8, 6, 7, 252, 6, 9]), 6);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getArrayMinElement(object), -1);
  });
});

describe("arrayMaxElement find maximum element of array", function () {

  it("in array [9, 8, 6, 7, 2, 6, 9] - maximum element - 9", function () {
    assert.equal(getArrayMaxElement([9, 8, 6, 7, 2, 6, 9]), 9);
  });
  it("in array [10, 8, 6, 7,252, 6, 9] - maximum element - 252", function () {
    assert.equal(getArrayMaxElement([10, 8, 6, 7, 252, 6, 9]), 252);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getArrayMaxElement(object), -1);
  });
});

describe("minElementIndex find index of minimal element of array", function () {

  it("in array [9, 8, 6, 7, 2, 6, 9] - index of minimal element - 4", function () {
    assert.equal(getMinElementIndex([9, 8, 6, 7, 2, 6, 9]), 4);
  });
  it("in array [10, 8, 6, 7,252, 6, 10] - index of minimal element - 2", function () {
    assert.equal(getMinElementIndex([10, 8, 6, 7, 252, 6, 10]), 2);
  });
  it("in array [10, 8, 6, 7, -12, 6, 10] - index of minimal element - 4", function () {
    assert.equal(getMinElementIndex([10, 8, 6, 7, -12, 6, 10]), 4);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getMinElementIndex(object), -1);
  });
});

describe("minElementIndex find index of maximum element of array", function () {

  it("in array [9, 8, 6, 7, 2, 6, 9] - index of maximum element - 0", function () {
    assert.equal(getMaxElementIndex([9, 8, 6, 7, 2, 6, 9]), 0);
  });
  it("in array [10, 8, 6, 7,252, 6, 10] - index of maximum element - 4", function () {
    assert.equal(getMaxElementIndex([10, 8, 6, 7, 252, 6, 10]), 4);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getMaxElementIndex(object), -1);
  });
});

describe("sumOfOddArray - sum of odd index element", function () {

  it("in array [9, 8, 6, 7, 2, 6, 9] - sum of odd index element - 26", function () {
    assert.equal(getSumOfOddArray([9, 8, 6, 7, 2, 6, 9]), 26);
  });
  it("in array [10, 8, 6, 7, 252, 6, 10] - sum of odd index element - 278", function () {
    assert.equal(getSumOfOddArray([10, 8, 6, 7, 252, 6, 10]), 278);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getSumOfOddArray(object), -1);
  });
});

describe("mirrorArray", function () {

  it("array [9, 8, 6, 7, 2, 6, 9] - reverse array - [9, 6, 2, 7, 6, 8, 9]", function () {
    assert.deepEqual(getMirrorArray([9, 8, 6, 7, 2, 6, 9]), [9, 6, 2, 7, 6, 8, 9]);
  });
  it("array [10, 8, 6, 7, 252, 6, 10] - reverse array - [10, 6, 252, 7, 6, 8, 10]", function () {
    assert.deepEqual(getMirrorArray([10, 8, 6, 7, 252, 6, 10]), [10, 6, 252, 7, 6, 8, 10]);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getMirrorArray(object), -1);
  });
});

describe("quantityOdd Count of odd elements", function () {

  it("array [9, 8, 6, 7, 2, 6, 9] - Count of odd elements - 3", function () {
    assert.equal(getQuantityOdd([9, 8, 6, 7, 2, 6, 9]), 3);
  });
  it("array [10, 8, 6, 7, 252, 6, 10] - Count of odd elements - 1", function () {
    assert.equal(getQuantityOdd([10, 8, 6, 7, 252, 6, 10]), 1);
  });
  it("array [10, 8, 6, 24, 252, 6, 10] - Count of odd elements - 0", function () {
    assert.equal(getQuantityOdd([10, 8, 6, 24, 252, 6, 10]), 0);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getQuantityOdd(object), -1);
  });
});

describe("halfReverseArray - reverse first half with second half", function () {

  it("array [9, 8, 6, 7, 2, 6, 9] - reverse of half array [2, 6, 9, 9, 8, 6, 7]", function () {
    assert.deepEqual(getHalfReverseArray([9, 8, 6, 7, 2, 6, 9]), [2, 6, 9, 9, 8, 6, 7]);
  });
  it("array [10, 8, 6, 7, 252, 6, 10] - Count of odd elements - 1", function () {
    assert.deepEqual(getHalfReverseArray([10, 8, 6, 7, 252, 6, 10, 8]), [252, 6, 10, 8, 10, 8, 6, 7]);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.equal(getHalfReverseArray(object), -1);
  });
});

describe("bubbleSort", function () {
  it("array [9, 8, 6, 7, 2, 6, 9] sort [ 2, 6, 6, 7, 8, 9, 9 ]", function () {
    assert.deepEqual(getBubbleSort([9, 8, 6, 7, 2, 6, 9]), [ 2, 6, 6, 7, 8, 9, 9 ]);
  });
  it("array [10, 8, 6, 7, 252, 6, 10] - [ 6, 6, 7, 8, 8, 10, 10, 252 ]", function () {
    assert.deepEqual(getBubbleSort([10, 8, 6, 7, 252, 6, 10, 8]), [ 6, 6, 7, 8, 8, 10, 10, 252 ]);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.deepEqual(getBubbleSort(object), -1);
  });
});

describe("selectSort", function () {

  it("array [10, 8, 6, 7, 252, 6, 10] - [ 6, 6, 7, 8, 8, 10, 10, 252 ]", function () {
    assert.deepEqual(selectSort([10, 8, 11, 1, 252, 6, 10, 9]), [1, 6, 8, 9, 10, 10, 11, 252 ]);
  });
  it("array [10, 8, 6, 7, 252, 6, 10] - [ 6, 6, 7, 8, 8, 10, 10, 252 ]", function () {
    assert.deepEqual(selectSort([10, 8, 6, 1, 252, 6, 10, 8]), [1, 6, 6, 8, 8, 10, 10, 252 ]);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.deepEqual(selectSort(object), -1);
  });
});

describe("insertSort", function () {

  it("array [9, 8, 6, 7, 2, 6, 9] sort [ 5, 6, 7, 8, 15, 17, 32 ]", function () {
    assert.deepEqual(insertSort([5, 8, 32, 7, 17, 6, 15]), [ 5, 6, 7, 8, 15, 17, 32 ]);
  });
  it("array [10, 8, 6, 7, 252, 6, 10] - [ 6, 6, 7, 8, 8, 10, 10, 252 ]", function () {
    assert.deepEqual(insertSort([2, 8, 3, 7, 252, 6, 10, 11]), [2, 3, 6, 7, 8, 10, 11, 252 ]);
  });
  it("input is object {name: 'Alex', lastname: 'Morgan'} - output -1", function () {
    assert.deepEqual(insertSort(object), -1);
  });
});