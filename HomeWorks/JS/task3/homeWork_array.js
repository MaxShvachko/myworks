function getArrayMinElement(newArray) {
    if(!(Array.isArray(newArray))) {
        return -1;
    }
    var result = newArray[0];
    for (var i = 0; i < newArray.length; i++) {
        if (result > newArray[i]) {
            result = newArray[i];
        }
    }
    return result;
}

function getArrayMaxElement(newArray) {
    if(!Array.isArray(newArray)) {
        return -1;
    }

    var result = newArray[0];

    for (var i = 0; i < newArray.length; i++) {
        if (result < newArray[i]) {
            result = newArray[i];
        }
    }
    return result;
}

function getMinElementIndex(newArray) {
    if(!Array.isArray(newArray)) {
        return -1;
    }
    var index = 0;

    for (var i = 1; i < newArray.length; i++) {
        if (newArray[index] > newArray[i]) {
            index = i;
        }
    }
    return index;
}

function getMaxElementIndex(newArray) {
    if(!Array.isArray(newArray)) {
        return -1;
    }

    var result = newArray[0];
    var index = 0;

    for (var i = 0; i < newArray.length; i++) {
        if (result < newArray[i]) {
            index = i;
        }
    }
    return index;
}

function getSumOfOddArray(newArray) {
    if(!Array.isArray(newArray)) {
        return -1;
    }

    var sum = 0;

    for (var i = 0; i < newArray.length; i += 2) {
        sum += newArray[i];
    }
    return sum;
}

function getMirrorArray(newArray) {
    if(!Array.isArray(newArray)) {
        return -1;
    }

    var index = 0;
    var mirror = [];
    var lengthArray = newArray.length;
    for (var i = lengthArray - 1; i >= 0; i--) {
        mirror[index] = newArray[i];
        index++;
    }
    return mirror;
}

function getQuantityOdd(newArray) {
    if(!Array.isArray(newArray)) {
        return -1;
    }
    var count = 0;

    for (var i = 0; i < newArray.length; i++) {
        if (newArray[i] % 2 !== 0) {
            count++;
        }
    }
    return count;
}

function getHalfReverseArray(newArray) {
    if(!Array.isArray(newArray)) {
        return -1;
    }
    var firstHalf = [];
    var secondHalf = [];
    var result;

    for (var j = newArray.length - 1; j > (newArray.length - 1) / 2; j--) {
        firstHalf.unshift(newArray[j]);
    }
    for (var i = 0; i < newArray.length / 2; i++) {
        secondHalf.push(newArray[i]);
    }
    result = firstHalf.concat(secondHalf);
    return result;
}

function getBubbleSort(array) {
    if (!Array.isArray(array)) {
        return -1;
    }
    var lengthCircle = array.length - 1;

    for (var i = 0; i < lengthCircle; i++) {
        for (var j = 0; j < lengthCircle; j++) {
            if (array[j] > array[j +1]) {
                var arrayValue = array[j];
                array[j] =  array[j +1];
                array[j + 1] = arrayValue;
            }
        }
    }
    return array;
}

function selectSort(array) {
    if(!Array.isArray(array)) {
        return -1;
    }
    for(var i = 0; i < array.length; i++){
        var minElement = i;
        for (var j = i + 1; j < array.length; j++) {
            if (array[j] < array[minElement]) {
                minElement = j;
            }
            if (minElement !== i) {
                var temp = array[i];
                array[i] = array[minElement];
                array[minElement] = temp;
            }
        }
    }
    return array;
}

function insertSort(array) {
    if(!Array.isArray(array)) {
        return -1;
    }
        let length = array.length;
        for (let i = 1; i < length; i++) {
            let key = array[i];
            let j = i - 1;
            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
        return array;
    };

module.exports = {
    getArrayMinElement: getArrayMinElement,
    getArrayMaxElement: getArrayMaxElement,
    getMinElementIndex: getMinElementIndex,
    getMaxElementIndex: getMaxElementIndex,
    getSumOfOddArray: getSumOfOddArray,
    getMirrorArray: getMirrorArray,
    getQuantityOdd: getQuantityOdd,
    getHalfReverseArray: getHalfReverseArray,
    getBubbleSort: getBubbleSort,
    selectSort: selectSort,
    insertSort: insertSort,
};


