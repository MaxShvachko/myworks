//evenOdd function
const assert  = require('chai').assert;
const getCheckNumber  = require('HW/task1/index').assert;


function getEvenOdd(a, b) {
  getCheckNumber(a);
  getCheckNumber(b);

  if (a % 2 === 0) {
    return a * b;
  } else {
    return a + b;
  }
}

describe("evenOdd четное", function () {

  it("если а четное то а * в", function () {
    assert.equal(getEvenOdd(2, 3), 6);
  });
it("если а нечетное то а + в", function () {
    assert.equal(getEvenOdd(3, 3), 6);
  });
});

// //checkNumber function
// //
// describe("getCheckNumber", function () {
//
//   it("Если ввести строку выводит false", function () {
//     assert.equal(getCheckNumber("a", 3), -1);
//   });
//   it("Если ввести обьект выводит false", function () {
//     assert.equal(getCheckNumber([5,6], 3), -1);
//   });
//   it("Если ввести null выводит false", function () {
//     assert.equal(getCheckNumber(null, 3), -1);
//   });
//   it("Если ввести 5, 7, 8 выводит 1", function () {
//     assert.equal(getCheckNumber(5, 7, 8), 1);
//   });
// });
//
// // //coordinates
// describe("coordinates", function () {
//
//   it("Если ввести x = 2 y = 2 - четверть 1", function () {
//     assert.equal(getQuarterByCoordinates(2, 2), "\nquarter I");
//   });
//   it("Если ввести x = 0 y = 0 - в начале координат", function () {
//     assert.equal(getQuarterByCoordinates(0, 0), "\nstart of coordinates");
//   });
//   it("Если ввести x = -2 y = 2 - четверть 2", function () {
//     assert.equal(getQuarterByCoordinates(-2, 2), "\nquarter II");
//   });
//   it("Если ввести x = -2 y = -2 - четверть 3", function () {
//     assert.equal(getQuarterByCoordinates(-2, -2), "\nquarter III");
//   });
//   it("Если ввести x = 2 y = -2 - четверть 4", function () {
//     assert.equal(getQuarterByCoordinates(2, -2), "\nquarter IV");
//   });
//   it("Если ввести x = 0  y = 5 - точка между 1 и 2 четветью", function () {
//     assert.equal(getQuarterByCoordinates(0, 5), "\n X on axis x. Point between quarter I and quater II");
//   });
//   it("Если ввести x = 0  y = -5 - точка между 3 и 4 четвертью", function () {
//     assert.equal(getQuarterByCoordinates(0, -5), "\nX on axis x. Point between quarter III and quater IV");
//   });
//   it("Если ввести x = 5 y = 0 - точка между 1 и ч четвертью", function () {
//     assert.equal(getQuarterByCoordinates(5, 0), "\nY on axis y. Point between quarter I and quater IV");
//   });
//   it("Если ввести x = -5 y = 0 - точка между 2 и 3 четвертью", function () {
//     assert.equal(getQuarterByCoordinates(-5, 0), "\nY on axis y. Point between quarter II and quater III");
//   });
// });
//
//
// //sumPositive function
//
// describe("sumPositive Найти суммы только положительных из трех чисел", function () {
//
//   it("если а = 5, b = 5, c = 5 ответ 15", function () {
//     assert.equal(sumPositive(5, 5, 5), 15);
//   });
//   it("если а = 5, b = -5, c = 5 - ответ 10", function () {
//     assert.equal(sumPositive(5,-5, 5), 10);
//   });
// });
//
// // 4 challenge (calculation max(а*б*с, а+б+с)+3)
//
// describe("calculationMax \nПосчитать выражение max(а*б*с, а+б+с)+3", function () {
//
//   it("если a = 5, b = 5, c = 5  + 3 - ответ 15 (проверка умножения)", function () {
//     assert.equal(calculationMax(5, 5, 5), 128);
//   });
//   it("если а = 2, b = 1, c = 2 + 3 - ответ 8 (проверка сложения)", function () {
//     assert.equal(calculationMax(2,1,2), 8);
//   });
// });
//
// // studentGrades
//
// describe("studentGrades (определить оценку студента по его балу) ", function () {
//   describe("Оценка F ", function () {
//     it("если Оценка студента 0", function () {
//       assert.equal(studentGrades(0), "Оценка F");
//     });
//     it("если Оценка студента 13", function () {
//       assert.equal(studentGrades(13), "Оценка F");
//     });
//     it("если Оценка студента 19", function () {
//       assert.equal(studentGrades(19), "Оценка F");
//     });
//    });
//
//   describe("Оценка E ", function () {
//     it("если Оценка студента 20", function () {
//       assert.equal(studentGrades(20), "Оценка E");
//     });
//     it("если Оценка студента 27", function () {
//       assert.equal(studentGrades(27), "Оценка E");
//     });
//     it("если Оценка студента 39", function () {
//       assert.equal(studentGrades(39), "Оценка E");
//     });
//   });
//
//   describe("Оценка D ", function () {
//     it("если Оценка студента 40", function () {
//       assert.equal(studentGrades(40), "Оценка D");
//     });
//     it("если Оценка студента 48", function () {
//       assert.equal(studentGrades(48), "Оценка D");
//     });
//     it("если Оценка студента 59", function () {
//       assert.equal(studentGrades(59), "Оценка D");
//     });
//   });
//
//   describe("Оценка C ", function () {
//     it("если Оценка студента 60", function () {
//       assert.equal(studentGrades(60), "Оценка C");
//     });
//     it("если Оценка студента 67", function () {
//       assert.equal(studentGrades(72), "Оценка C");
//     });
//     it("если Оценка студента 74", function () {
//       assert.equal(studentGrades(74), "Оценка C");
//     });
//   });
//
//   describe("Оценка B ", function () {
//     it("если Оценка студента 75", function () {
//       assert.equal(studentGrades(75), "Оценка B");
//     });
//     it("если Оценка студента 82", function () {
//       assert.equal(studentGrades(82), "Оценка B");
//     });
//     it("если Оценка студента 89", function () {
//       assert.equal(studentGrades(89), "Оценка B");
//     });
//   });
//
//   describe("Оценка A", function () {
//     it("если Оценка студента 90", function () {
//       assert.equal(studentGrades(90), "Оценка A");
//     });
//     it("если Оценка студента 95", function () {
//       assert.equal(studentGrades(95), "Оценка A");
//     });
//     it("если Оценка студента 100", function () {
//       assert.equal(studentGrades(100), "Оценка A");
//     });
//   });
//
//   describe("getStudentGrades", function () {
//     it("should return error when parameter equals -25", function () {
//       var errorMessage = new Error("Рейтинг должен быть в диапозоне от 0 до 100 балов")
//       assert.equal(getStudentGrades(-25), errorMessage);
//     });
//     it("если Оценка студента 125", function () {
//       assert.equal(getStudentGrades(125), "Рейтинг должен быть в диапозоне от 0 до 100 балов");
//     });
//     it("если Оценка студента adf", function () {
//       assert.equal(getStudentGrades("adf"), -1);
//     });
//   });
// });
//
//


