function getCheckNumber (a) {
    if (typeof a !== "number") {
        return -1;
    }
    return 1
}

function getEvenOdd(a, b) {
    getCheckNumber(a);
    getCheckNumber(b);

    if (a % 2 === 0) {
        return a * b;
    } else {
        return a + b;
    }
}

function getQuarterByCoordinates(x, y) {
    getCheckNumber(x);
    getCheckNumber(y);

    var message = "";

    if (x > 0 && y > 0) {
        message = "quarter I";
    } else if (x < 0 && y > 0) {
        message = "quarter II";
    } else if (x < 0 && y < 0) {
        message = "quarter III";
    } else if (x > 0 && y < 0) {
        message = "quarter IV";
    } else if (x === 0 && y === 0) {
        message = "start of coordinates";
    }

    else if (x === 0 && y > 0) {
        message = "X on axis x. Point between quarter I and quater II";
    } else if (y < 0) {
        message = "X on axis x. Point between quarter III and quater IV";
    }else if (y === 0 && x > 0) {
        message = "Y on axis y. Point between quarter I and quater IV";
    }else {
        message = "Y on axis y. Point between quarter II and quater III";
    }
    return message;
}

function getSumPositive(a, b, c) {
    getCheckNumber(a);
    getCheckNumber(b);
    getCheckNumber(c);

    var sum = 0;

    if (a >= 0) {
        sum += a;
    }
    if (b >= 0) {
        sum += b;
    }
    if (c >= 0) {
        sum += c;
    }
    return sum;
}

function getCalculationMax(a, b, c) {
    if (typeof (a) !== "number" || (typeof (b) !== "number") || (typeof (c) !== "number")) {
        return -1;
    }
    var sumNumbers = a + b + c;
    var multiplyNumbers = a * b * c;

    if (sumNumbers > multiplyNumbers) {
        return sumNumbers + 3;
    } else {
        return multiplyNumbers + 3;
    }
}

function getStudentGrades(grade) {
    if (grade < 0 || grade > 100 || !(typeof grade === 'number')) {
        return ("Rating should be in the range from 0 to 100 points");
    }

    if (grade >= 0 && grade <= 19) {
        return "Grade F";
    } else if (grade <= 39) {
        return "Grade E";
    } else if (grade <= 59) {
        return "Grade D";
    } else if (grade <= 74) {
        return "Grade C";
    } else if (grade <= 89) {
        return "Grade B";
    } else {
        return "Grade A";
    }
}

module.exports = {
    getCheckNumber,
    getEvenOdd,
    getQuarterByCoordinates,
    getSumPositive,
    getCalculationMax,
    getStudentGrades
};








