// //evenOdd function
const assert  = require('chai').assert;
const app = require('../index');

getQuarterByCoordinates = app.getQuarterByCoordinates;
getSumPositive = app.getSumPositive;
getCalculationMax = app.getCalculationMax;
getStudentGrades = app.getStudentGrades;

describe("getEvenOdd ", function () {
  it("if number is even а * в", function () {
    assert.equal(app.getEvenOdd(2, 3), 6);
  });
it("if number is even а + в", function () {
    assert.equal(app.getEvenOdd(3, 3), 6);
  });
});

describe("getCheckNumber", function () {

  it("if input a string output - false", function () {
    assert.equal(app.getCheckNumber("a", 3), -1);
  });
  it("if input an object output - false", function () {
    assert.equal(app.getCheckNumber([5,6], 3), -1);
  });
  it("if input a null output false", function () {
    assert.equal(app.getCheckNumber(null, 3), -1);
  });
  it("if input 5, 7, 8 output 1", function () {
    assert.equal(app.getCheckNumber(8), 1);
  });
});

describe("getQuarterByCoordinates (function search what quarter belong a dot)", function () {

  it("x = 2 y = 2 - quarter I", function () {
    assert.equal(getQuarterByCoordinates(2, 2), "quarter I");
  });
  it("x = 0 y = 0 - start of coordinates", function () {
    assert.equal(getQuarterByCoordinates(0, 0), "start of coordinates");
  });
  it("x = -2 y = 2 - quarter II", function () {
    assert.equal(getQuarterByCoordinates(-2, 2), "quarter II");
  });
  it("x = -2 y = -2 - quarter III", function () {
    assert.equal(getQuarterByCoordinates(-2, -2), "quarter III");
  });
  it("x = 2 y = -2 - quarter IV", function () {
    assert.equal(getQuarterByCoordinates(2, -2), "quarter IV");
  });
  it("x = 0  y = 5 - X on axis x. Point between quarter I and quater II", function () {
    assert.equal(getQuarterByCoordinates(0, 5), "X on axis x. Point between quarter I and quater II");
  });
  it("x = 0  y = -5 - X on axis x. Point between quarter III and quater IV", function () {
    assert.equal(getQuarterByCoordinates(0, -5), "X on axis x. Point between quarter III and quater IV");
  });
  it("x = 5 y = 0 - Y on axis y. Point between quarter I and quater IV", function () {
    assert.equal(getQuarterByCoordinates(5, 0), "Y on axis y. Point between quarter I and quater IV");
  });
  it("x = -5 y = 0 - Y on axis y. Point between quarter II and quater III", function () {
    assert.equal(getQuarterByCoordinates(-5, 0), "Y on axis y. Point between quarter II and quater III");
  });
});

describe("getSumPositive (function find only sum of positive number) ", function () {

  it("if input а = 5, b = 5, c = 5 output 15", function () {
    assert.equal(getSumPositive(5, 5, 5), 15);
  });
  it("if а = 5, b = -5, c = 5 - output 10", function () {
    assert.equal(getSumPositive(5,-5, 5), 10);
  });
});

describe("getCalculationMax - calculate max(а*б*с, а+б+с)+3", function () {

  it("if a = 5, b = 5, c = 5  + 3 - output 15 (multiplication check)", function () {
    assert.equal(getCalculationMax(5, 5, 5), 128);
  });
  it("if а = 2, b = 1, c = 2 + 3 - output 8 (addition check)", function () {
    assert.equal(getCalculationMax(2,1,2), 8);
  });
  it("check type of arguments (if arg type string return -1)", function () {
    assert.equal(getCalculationMax("Hello world",1,2), -1);
  });
});

describe("getStudentGrades (determine student's grade by his score) ", function () {
  describe("Оценка F ", function () {
    it("if rating 0", function () {
      assert.equal(getStudentGrades(0), "Grade F");
    });
    it("if rating 13", function () {
      assert.equal(getStudentGrades(13), "Grade F");
    });
    it("if rating 19", function () {
      assert.equal(getStudentGrades(19), "Grade F");
    });
   });

  describe("Оценка E ", function () {
    it("rating 20", function () {
      assert.equal(getStudentGrades(20), "Grade E");
    });
    it("rating 27", function () {
      assert.equal(getStudentGrades(27), "Grade E");
    });
    it("rating 39", function () {
      assert.equal(getStudentGrades(39), "Grade E");
    });
  });

  describe("Grade D ", function () {
    it("rating 40", function () {
      assert.equal(getStudentGrades(40), "Grade D");
    });
    it("rating 48", function () {
      assert.equal(getStudentGrades(48), "Grade D");
    });
    it("rating 59", function () {
      assert.equal(getStudentGrades(59), "Grade D");
    });
  });

  describe("Grade C ", function () {
    it("rating 60", function () {
      assert.equal(getStudentGrades(60), "Grade C");
    });
    it("rating 67", function () {
      assert.equal(getStudentGrades(72), "Grade C");
    });
    it("rating 74", function () {
      assert.equal(getStudentGrades(74), "Grade C");
    });
  });

  describe("Grade B ", function () {
    it("rating 75", function () {
      assert.equal(getStudentGrades(75), "Grade B");
    });
    it("rating 82", function () {
      assert.equal(getStudentGrades(82), "Grade B");
    });
    it("rating 89", function () {
      assert.equal(getStudentGrades(89), "Grade B");
    });
  });

  describe("Grade A", function () {
    it("rating 90", function () {
      assert.equal(getStudentGrades(90), "Grade A");
    });
    it("rating 95", function () {
      assert.equal(getStudentGrades(95), "Grade A");
    });
    it("rating 100", function () {
      assert.equal(getStudentGrades(100), "Grade A");
    });
  });

  describe("getStudentGrades errors", function () {
    var errorMessage = "Rating should be in the range from 0 to 100 points";
    it("should return error when parameter equals -25", function () {
      assert.deepEqual(getStudentGrades(-25), errorMessage);
    });
    it(`student rating 125 - ${errorMessage}`, function () {
      assert.deepEqual(getStudentGrades(125), errorMessage);
    });
    it(`если Оценка студента adf - ${errorMessage}`, function () {
      assert.deepEqual(getStudentGrades("adf"), errorMessage);
    });
  });
});




