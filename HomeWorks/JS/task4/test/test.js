const assert = require('chai').assert;
const app = require('../homeWork_function');

dayNameByNumber = app.dayNameByNumber;
getDecardLong = app.getDecardLong;
convertNumberIntoWord = app.convertNumberIntoWord;
convertWordIntoNumber = app.convertWordIntoNumber;

describe("dayNameByNumber output Name day for number", function() {
  it ("Day number 1 is Monday", function () {
    assert.equal(dayNameByNumber(1), "Monday");
  });
  it ("Day number 7 is Sunday", function () {
    assert.equal(dayNameByNumber(7), "Sunday");
  });
  it ("Day number 5 is Friday", function () {
    assert.equal(dayNameByNumber(5), "Friday");
  });

  it ("Enter day number -1 - output Enter number in range 1 - 7", function () {
    assert.equal(dayNameByNumber(-1), "Enter number in range 1 - 7");
  })
  it ("Enter day number 8 - output Enter number in range 1 - 7", function () {
    assert.equal(dayNameByNumber(8), "Enter number in range 1 - 7");
  })
  it ("Enter day number \'number\' - output enter a number", function () {
    assert.equal(dayNameByNumber("number"), "enter a number");
  })
})

describe("getDecardLong find long between x and y", function() {
  it ("if x1 = 4 y1 = 4, x2 = -1 y2 = -1 - long between x and y = 7.07", function () {
    assert.equal(getDecardLong( 4, -1, 4,-1), 7.07);
  });
  it ("if x1 = -3 y1 = 7, x2 = 6 y2 = -2 - long between x and y = 12.72", function () {
    assert.equal(getDecardLong( -3, 6, 7,-2), 12.72);
  });
})

describe("numberIntoWord translate number into word", function() {
  it ("Convert 0 into a number - Zero", function () {
    assert.equal(convertNumberIntoWord( 0), "Zero");
  });
  it ("Convert 12 into a number - Twelve", function () {
    assert.equal(convertNumberIntoWord( 12), "Twelve");
  });
  it ("Convert 35 into a number - Thirty Five", function () {
    assert.equal(convertNumberIntoWord( 35), "Thirty Five");
  });
  it ("Convert 40 into a number - Forty", function () {
    assert.equal(convertNumberIntoWord( 40), "Forty");
  });
  it ("Convert 60 into a number - Sixty", function () {
    assert.equal(convertNumberIntoWord( 60), "Sixty");
  });
  it ("Convert 500 into a number - Five hundred", function () {
    assert.equal(convertNumberIntoWord( 500), "Five hundred");
  });
  it ("Convert 753 into a number - Seven hundred Fifty Three", function () {
    assert.equal(convertNumberIntoWord( 753), "Seven hundred Fifty Three");
  });
  it ("Convert 999 into a number - Nine hundred Ninety Nine", function () {
    assert.equal(convertNumberIntoWord( 999), "Nine hundred Ninety Nine");
  });
  it ("argument is not a number - return -1", function () {
    assert.equal(convertNumberIntoWord("abc"), -1);
  });
  it ("Convert 209 into a word - Two hundred nine", function () {
    assert.equal(convertNumberIntoWord(209), "Two hundred Nine");
  });
  it ("Convert 212 into a word - Two hundred Twelve", function () {
    assert.equal(convertNumberIntoWord(212), "Two hundred Twelve");
  });
  it ("Convert 220 into a word - Two hundred Twenty", function () {
    assert.equal(convertNumberIntoWord(230), "Two hundred Thirty");
  });
});

describe("convertWordIntoNumber translate word into number", function() {
  it ("Convert Zero into a number - 0", function () {
    assert.equal(convertWordIntoNumber( "Zero"), 0);
  });
  it ("Convert Twelve into a number - Twelve", function () {
    assert.equal(convertWordIntoNumber( "Twelve"), 12);
  });
  it ("Convert Thirty Five into a number - 35", function () {
    assert.equal(convertWordIntoNumber( "Thirty Five"), 35);
  });
  it ("Convert Forty into a number - 40", function () {
    assert.equal(convertWordIntoNumber( "Forty"), 40);
  });
  it ("Convert Sixty into a number - 60", function () {
    assert.equal(convertWordIntoNumber( "Sixty"), 60);
  });
  it ("Convert Five hundred into a number - 500", function () {
    assert.equal(convertWordIntoNumber( "Five hundred"), 500);
  });
  it ("Convert Seven hundred Fifty Three into a number - 753", function () {
    assert.equal(convertWordIntoNumber( "Seven hundred Fifty Three"), 753);
  });
  it ("Convert Nine hundred Ninety Nine into a number - 999", function () {
    assert.equal(convertWordIntoNumber( "Nine hundred Ninety Nine"), 999);
  });
  it ("argument isn`t string return -1", function () {
    assert.equal(convertWordIntoNumber( [1, 2, 3]), -1);
  });
  it ("Convert Nine hundred twelve into a number - 912", function () {
    assert.equal(convertWordIntoNumber( "Nine hundred twelve"), 912);
  });
  it ("Convert One hundred into a number - 100", function () {
    assert.equal(convertWordIntoNumber( "One hundred"), 100);
  });
  it ("Convert One hundred one a number - 100", function () {
    assert.equal(convertWordIntoNumber( "One hundred one"), 101);
  });
  it ("Convert One hundred forty a number - 140", function () {
    assert.equal(convertWordIntoNumber( "One hundred forty"), 140);
  });
  it ("Convert One hundred forty a number - 140", function () {
    assert.equal(convertWordIntoNumber( "One hundred forty"), 140);
  });
});