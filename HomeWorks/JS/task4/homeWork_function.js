function dayNameByNumber(dayNumber) {
    if (dayNumber <= 0 || dayNumber >= 8) {
        return "Enter number in range 1 - 7";
    } else if (typeof dayNumber !== "number") {
        return "enter a number";
    }

    var week = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    ];
    return week[dayNumber - 1];
}

function getDecardLong (x1, x2, y1, y2) {
    var result = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
    return ((parseInt(result * 100)) / 100);
}

function convertNumberIntoWord(number) {
    if (typeof number !== "number" || number < 0) {
        return -1;
    }
    var temp = number;
    var result = "";
    var unitText = [
        "Zero",
        "One",
        "Two",
        "Three",
        "Four",
        "Five",
        "Six",
        "Seven",
        "Eight",
        "Nine",
        "Ten"
    ];
    var unitTen = [
        "Eleven",
        "Twelve",
        "Thirteen",
        "Fourteen",
        "Fifteen",
        "Sixteen",
        "Seventeen",
        "Eighteen",
        "Nineteen",
    ];
    var  tensText = [
        "Twenty",
        "Thirty",
        "Forty",
        "Fifty",
        "Sixty",
        "Seventy",
        "Eighty",
        "Ninety",
    ];

    if(temp <= 10) {
        result = unitText[temp];
    }else if (temp > 10 && temp < 20) {
        result = unitTen[temp - 11];
    }else if (temp >= 20 && temp < 100) {
        if (temp % 10 === 0) {
            result = tensText[Math.floor(temp / 10) - 2]
        }else {
            result = tensText[Math.floor(temp / 10) - 2] + " " + unitText[temp % 10];
        }
    }else if (temp >= 100 && temp < 1000) {
        result = unitText[Math.floor(temp / 100)] + " hundred";
        if (temp % 100 === 0) {
            result = result;
        }else if (temp % 100 <= 10) {
            result += " " + unitText[temp % 100];
        }else if (temp % 100 > 10 && temp % 100 < 20) {
            result += " " + unitTen[temp % 100 - 11];
        }else if (temp % 100 >= 20 && temp % 100 < 100) {
            if (temp % 10 === 0) {
                result += " " + tensText[Math.floor((temp % 100) / 10) - 2];
            }
            else{
                result += " " + tensText[Math.floor((temp % 100) / 10) - 2] + " " + unitText[temp % 10];
            }
        }
    }
    return result;
}
function convertWordIntoNumber(word) {
    if (typeof word !== "string") {
        return -1;
    }
    var numberOfWord = 0;
    var temp = word.toLowerCase();
    var tempList;
    var unitText = [
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
        "ten"
    ];
    var unitTen = [
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen",
    ];
    var tensText = [
        "twenty",
        "thirty",
        "forty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety",
    ];
    tempList = temp.split(" ");

    for (var i = 0; i < unitText.length; i++) {
        if (tempList.length === 1) {
            if (tempList[0] === unitText[i]) {
                numberOfWord += i;
            } else if (tempList[0] === unitTen[i]) {
                numberOfWord += i + 11;
            } else if (tempList[0] === tensText[i]) {
                numberOfWord += (i + 2) * 10;
            }
        }else if (tempList.length === 2) {
            if (tempList[1] === "hundred") {
                if (tempList[0] === unitText[i]) {
                    numberOfWord += i * 100;
                }
            }else if (tempList[0] === tensText[i]) {
                numberOfWord += ((i + 2) * 10);
            }if (tempList[1] === unitText[i]) {
                numberOfWord += i;
            }
        }else if (tempList.length === 3) {
            if (tempList[0] === unitText[i]) {
                numberOfWord += i * 100;
            }if (tempList[2] === unitText[i]) {
                numberOfWord += i;
            }if (tempList[2] === unitTen[i]) {
                numberOfWord += i + 11;
            }if (tempList[2] === tensText[i]) {
                numberOfWord += (i + 2) * 10;
            }
        }else if (tempList.length === 4) {
            if (tempList[0] === unitText[i]) {
                numberOfWord += i * 100;
            }else if (tempList[2] === tensText[i]) {
                numberOfWord += (i + 2) * 10;
            }if (tempList[3] === unitText[i]) {
                numberOfWord += i;
            }
        }
    }
    return numberOfWord;
}

module.exports = {
    dayNameByNumber: dayNameByNumber,
    getDecardLong: getDecardLong,
    convertNumberIntoWord: convertNumberIntoWord,
    convertWordIntoNumber: convertWordIntoNumber,
};


