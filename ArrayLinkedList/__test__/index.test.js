var app = require('../index');

describe('Array init', () => {
    test('Array init is', () => {
    app.res.init("25",5,2);
        expect(app.res.array).toEqual(["25",5,2]);
        app.res.array.length = 0;
    });
});

describe('Array getSize', () => {
    test('Array getSize is', () => {
        app.res.init("25", 5, 2, 7);
        var action = app.res.getSize();
        var expected = 4;
        expect(action).toEqual(expected);
        app.res.array.length = 0;
    });
});

describe('Array push', () => {
    test('Array push is', () => {
        app.res.push(5);
        var expected = [5];
        expect(app.res.array).toEqual(expected);
        app.res.array.length = 0;
    });
    test('Array push is length', () => {
        app.res.init(1,5,6);
        var action = app.res.push(5,7,6,9);
        var expected = 7;
        expect(action).toEqual(expected);
    });
});

describe('Array pop', () => {
    test('Array pop is', () => {
        app.res.init(2 ,5, 2);
        app.res.pop();
        var expected = [2, 5];
        expect(app.res.array).toEqual(expected);
        app.res.array.length = 0;
    });
    test('Array pop return', () => {
        app.res.init(2 ,5, 2);
        var actual = app.res.pop();
        var expected = 2;
        expect(actual).toEqual(expected);
    });
});

describe('Array shift', () => {
    test('Array shift is', () => {
        app.res.init(2 ,5, 2);
        var expected = 2;
        console.log(app.res.array);
        expect(app.res.shift()).toBe(expected);
    });
});

describe('Array unshift', () => {
    test('Array shift is', () => {
        app.res.init(2 ,5, 2);
        var expected = 6;
        expect(app.res.unshift(2,4,8)).toBe(expected);
        app.res.array.length = 0;
    });
    test('Array unshift check array is', () => {
        app.res.init(2 ,5, 2);
        var expected = [2, 4, 8, 2, 5, 2];
        app.res.unshift(2, 4, 8);
        expect(app.res.array).toEqual(expected);
    });
});

describe('Array slice', () => {
    test('Array slice is', () => {
        app.res.init(2 ,5, 2, 5, 8, 6, 1, 3, 6, 6);
        var expected = [2, 5];
        expect(app.res.slice(2, 4)).toEqual(expected);
        app.res.array.length = 0;
    });
    test('Array slice check array is', () => {
        app.res.init(2 ,5, 2);
        var expected = [2 ,5, 2];
        app.res.slice();
        expect(app.res.array).toEqual(expected);
    });
});

describe('Array splice', () => {
    test('Array splice is', () => {
        app.res.init(2 ,5, 2, 5, 8, 6, 1, 3, 6, 6);
        var expected = [2, 5, 8];
        expect(app.res.splice(2, 4, 1,5,6,7)).toEqual(expected);
        app.res.array.length = 0;
    });
    test('Array splice check array is', () => {
        app.res.init(2 ,5, 2, 6, 98, 9);
        var expected = [2 ,61, 78, 789, 464, 98, 9];
        var action = app.res.splice(1, 3, 61,78,789,464);
        expect(app.res.array).toEqual(expected);
    });
});

describe('Array splice', () => {
    test('Array splice is', () => {
        app.res.init(2 ,5, 2, 5, 8, 6, 1, 3, 6, 6);
        var expected = [2, 5, 8];
        expect(app.res.splice(2, 4, 1,5,6,7)).toEqual(expected);
        app.res.array.length = 0;
    });
    test('Array splice check array is', () => {
        app.res.init(2 ,5, 2, 6, 98, 9);
        var expected = [2 ,61, 78, 789, 464, 98, 9];
        var action = app.res.splice(1, 3, 61,78,789,464);
        expect(app.res.array).toEqual(expected);
    });
});

describe('Array sort', () => {
    test('Array sort is', () => {
        app.res.init(54,47,98,456,31,6487,456);
        var action = app.res.sort();
        var expected = [47,  54,  98,
          31, 456, 456,
          6487];

        expect(action).toEqual(expected);
        app.res.array.length = 0;
    });
    test('Array sort check array is', () => {
        app.res.init(54,65,98,997,31,21,456);
        var  action = app.res.sort();
        var expected = [54, 65, 98, 31, 21, 456, 997];
        expect(action).toEqual(expected);
        app.res.array.length = 0;
    });
});

[]


//LinkedList
describe('linked init', () => {
    test('linked init is', () => {
        var action = app.linkNode.init([2, 6, 6 , 9, 5 ,6]);
        var expected = app.linkNode.root;
        console.log(app.linkNode.root);
        expect(action).toEqual(expected);

    });
});

describe('linked getSize', () => {
    test('linked getSize is', () => {
        app.linkNode.init([2, 6, 6 ,9,5 ,6]);
        var action = app.linkNode.getSize();
        var expected = 6;
        expect(action).toBe(expected);
    });
});

describe('linked unshift', () => {
    test('linked unshift is', () => {
        var action = app.linkNode.unshift(2);
        var expected = 1;
        expect(action).toBe(expected);
        app.linkNode.shift();
    });
    test('linked unshift return 7', () => {
        app.linkNode.init([2, 6, 6 ,9,5,6]);
        var action = app.linkNode.unshift(2);
        var expected = 7;
        expect(action).toBe(expected);
    });
});

describe('linked shift', () => {
    test('linked shift is', () => {
        app.linkNode.init([2,4,4]);
        var action = app.linkNode.shift();
        var expected = 2;
        expect(action).toBe(expected);
    });
});

describe('linked push', () => {
    test('linked push is', () => {
        app.linkNode.unshift([2]);
        var expected = 3;
        app.linkNode.push(6);
        expect(app.linkNode.push(6)).toBe(expected);
    });
});

describe('linked pop', () => {
    test('linked pop is', () => {
        app.linkNode.unshift([2]);
        app.linkNode.push(6);
        var expected = 6;
        expect(app.linkNode.pop()).toBe(expected);
    });
});

describe('linked slice', () => {
    test('linked slice is', () => {
        app.linkNode.init([1,2,3,4,5,6,7,8,9]);
        var action = app.linkNode.slice(2, 4);
        var expected = app.linkNode;
        expect(action).toBe(expected);
    });
});

describe('linked deleteAt', () => {
    test('linked deleteAt is', () => {
        app.linkNode.init([1,2,3,4,5,6,7,8,9]);
        var action = app.linkNode.deleteAt(4);
        var expected = app.linkNode.root;
        expect(action).toBe(expected);
    });
});

describe('linked splice', () => {
    test('linked splice is', () => {
        app.linkNode.init([1,2,3,4,5,6,7,8,9]);
        var action = app.linkNode.splice(1,4);
        var expected = app.linkNode.root;
        expect(action).toBe(expected);
    });
});

describe('linked get', () => {
    test('linked get is', () => {
        app.linkNode.init([1,2,3,4,5,6,7,8,9]);
        var action = app.linkNode.get(5);
        var expected = {
            "next": {
                "next": {
                    "next": {
                        "next": {
                            "next": null,
                            "value": 9
                        },
                        "value": 8
                    },
                    "value": 7
                },
                "value": 6
            },
            "value": 5
        };
        expect(action).toEqual(expected);
    });
    test('linked get not on array is', () => {
        app.linkNode.init([1,2,3,4,5,6,7,8,9]);
        var action = app.linkNode.get(25);
        var expected = null;
        expect(action).toEqual(expected);
    });
});

describe('linked set', () => {
    test('linked set is', () => {
        app.linkNode.init([1,2,3,4,5,6,7,8,9]);
        var action = app.linkNode.set(4, 25);
        var expected = {
            "next": {
                "next": {
                    "next": {
                        "next": {
                            "next": {
                                "next": {
                                    "next": {
                                        "next": {
                                            "next": {
                                                "next": null,
                                                "value": 9
                                            },
                                            "value": 8
                                        },
                                        "value": 7
                                    },
                                    "value": 6
                                },
                                "value": 5
                            },
                            "value": 25
                        },
                        "value": 4
                    },
                    "value": 3
                },
                "value": 2
            },
            "value": 1
        };
        expect(action).toEqual(expected);
    });
});


