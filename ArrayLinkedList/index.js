function Ilist() {
}

Ilist.prototype.init = function () {};
Ilist.prototype.getSize = function () {};
Ilist.prototype.push = function () {};
Ilist.prototype.pop = function () {};
Ilist.prototype.shift = function () {};
Ilist.prototype.unshift = function () {};
Ilist.prototype.slice = function () {};
Ilist.prototype.splice = function () {};
Ilist.prototype.sort = function () {};
Ilist.prototype.get = function () {};
Ilist.prototype.set = function () {};

function ArrayList() {
    Ilist.apply(this, arguments);
    this.array = [];
}

ArrayList.prototype = Object.create(Ilist.prototype);
ArrayList.prototype.constructor = ArrayList;

ArrayList.prototype.init = function () {
    for(var i = 0; i < arguments.length; i++){
        this.array.push(arguments[i]);
    }
};

ArrayList.prototype.getSize = function () {
    return this.array.length;
};

ArrayList.prototype.push = function(){
    var j = 0;
    var arrLen = this.array.length;
    while(j !== arguments.length) {
        this.array[arrLen + j] = arguments[j];
        j++;
    }
    return this.array.length;
};
ArrayList.prototype.pop = function () {
    var result = this.array[this.array.length - 1];
    this.array.length -= 1;
    return result;
};

ArrayList.prototype.shift = function () {
    if (this.array.length === 0) {
        return undefined;
    }
    var result = this.array[0];
    for(var i = 0; i < this.array.length; i++){
        this.array[i] = this.array[i + 1];
    }
    this.array.length -= 1;
    return result;
};

ArrayList.prototype.unshift = function () {
    var size = this.array.length;
    var argLen = arguments.length;
    for(var i = size - 1 ; i >= 0; i--){
        this.array[i + argLen] = this.array[i];
    }
    for(var j = 0; j < argLen; j++) {
        this.array[j] = arguments[j];
    }
    return this.array.length;
};

ArrayList.prototype.slice = function () {
    var resultArray = new ArrayList();
    var arrLen = this.array.length;
    for (var i = 0; i < arrLen; i++) {
        if(arguments.length === 0) {
            resultArray.push(this.array[i])
        }else if (arguments[0] > this.array.length) {
            resultArray.array = [];
        }else if (arguments.length === 1 && i >= arguments[0]) {
            resultArray.push(this.array[i]);
        }
        else if (i >= arguments[0] && i < arguments[1]) {
            resultArray.push(this.array[i]);
        }
    }
    return resultArray.array;
};

ArrayList.prototype.splice = function (start, deleteCount) {
    var resultArray = new ArrayList();
    var arrayLeng = this.array.length;

    if (start > 0) {
        for (var i = 0; i < start; i++)  {
            this.array.push(this.array[i]);
        }
    }
       for (var j = start; j <= deleteCount; j++) {
           {
               resultArray.push(this.array[j]);
       }
   }
   for (var argCounter = 2; argCounter < arguments.length; argCounter++) {
       this.array.push(arguments[argCounter]);
   }

   for (var k = deleteCount + 1; k < arrayLeng; k++) {
       this.array.push(this.array[k]);
   }
   for(var l = 0; l < arrayLeng; l++) {
       this.array.shift();
   }
    return resultArray.array;
};

ArrayList.prototype.sort = function () {
    var temp;
    var array = this.array;
    for (var i = 0; i < array.length; i++) {
        if (array[i] > array[i+1]) {
            temp = array[i+1];
            array[i+1] = array[i];
            array[i] = temp;
        }
    }
    return array;
};

var res = new ArrayList();

// LinkedList
var Node = function(value) {
    this.value = value;
    this.next = null;
};
var LinkedList = function() {
    Ilist.apply(this, arguments);
    this.root = null;
};
LinkedList.prototype = Object.create(Ilist.prototype);
LinkedList.prototype.constructor = LinkedList;

LinkedList.prototype.getSize = function() {
    var tempNode = this.root;
    var size = 0;

    while(tempNode !== null) {
        tempNode = tempNode.next;
        size++;
    }
    return size;
};
LinkedList.prototype.unshift = function(value) {
    var size = this.getSize();
    var node = new Node(value);
    node.next = this.root;
    this.root = node;
    return size + 1 ;
};

LinkedList.prototype.init = function(initialArray) {
    for (var i = initialArray.length - 1 ; i >= 0; i--) {
        this.unshift(initialArray[i]);
    }
    return this.root;
};

LinkedList.prototype.shift = function() {
    var node = new Node();
    if(!this.root){
        return;
    }
    node = this.root.value;
    this.root = this.root.next;
    return node;
};

LinkedList.prototype.get = function(index) {
    var tempNode = this.root;
    var size = 0;

    while(size !== index) {
        tempNode = tempNode.next;
        size++;
    }
    return tempNode;
};

LinkedList.prototype.push = function (value) {
    var tempNode = this.root;
    var size = this.getSize();
    if (tempNode === null) {
        this.root = new Node(value);
        return ++size;
    }
    while(tempNode !== null) {
        if (tempNode.next === null) {
            tempNode.next = new Node(value);
            return ++size;
        }
        tempNode = tempNode.next;
    }
};

LinkedList.prototype.pop = function() {
    if (this.root === null) {
        return undefined;
    }
    var tempNode = this.root;
    var size = this.getSize();
    while(tempNode !== null) {
        if (tempNode.next === null) {
            tempNode.next = this.root.next;
             --size;
            return tempNode.next.value;
        }
        tempNode = tempNode.next;
    }
};

LinkedList.prototype.slice = function(start, end) {
    var tempNode = this.root;
    tempNode = this.get(start);
    var newNode = new LinkedList();

    for(var i = start; i < end; i++) {
        newNode.push(tempNode.value);
        tempNode = tempNode.next;
    }
    return this;
};

LinkedList.prototype.getAt = function(index) {
    let counter = 0;
    let node = this.root;
    while(node) {
        if(counter === index) {
            return node;
        }
        counter++;
        node = node.next;
    }
    return null;
};

LinkedList.prototype.splice = function(start,end) {
    if (arguments.length === 1) {
        var node = this.get(start);
        var to = this.getSize();
        for (var i = start; i <= to; i++) {
            this.deleteAt(start);
        }
    }
    if (arguments.length === 2) {
        var node = this.get(start);
        var to = end;
        for (var i = start; i <= to; i++) {
            this.deleteAt(start);
        }
    }
    if (arguments.length > 2) {
        var argumentsList = new LinkedList();

        for(var i = 2; i < arguments.length; i++ ) {
            argumentsList.push(arguments[i]);
        }

        var deleteTopIndex = start + end ;

        var node = this.get(deleteTopIndex);
        for(var i = deleteTopIndex +1; i <= this.getSize();i++ ) {
            argumentsList.push(node.value);
            node = node.next;
        }

        var result = new LinkedList();
        var to  = this.getSize();
        for (var i = start; i <= to; i++) {
            if(i <= end) {
                result.push(this.root);
                this.root = this.root.next;
            }
            this.deleteAt(start);
        }

        var argNode = argumentsList.get(0);
        for (var i = 0; i < argumentsList.getSize(); i++) {

            this.push(argNode.value);
            argNode = argNode.next;
        }
    }
    return argumentsList;
};

LinkedList.prototype.deleteAt = function(index){
    if (!this.root) {
        this.root = new Node(data);
        return;
    }
    if (index === 0) {
        this.root = this.root.next;
        return;
    }
    const previous = this.getAt(index - 1);
    if (!previous || !previous.next) {
        return;
    }
    previous.next = previous.next.next;
    return this.root
};

LinkedList.prototype.get = function (value) {
    return this.getValue(this.root, value);
};

LinkedList.prototype.getValue = function (node ,value) {
    if (node === null || node.next === null) {
        return null;
    }
    if (node.value === value) {
        return node;
    }
    return this.getValue(node.next, value);
};

LinkedList.prototype.set = function (index, value) {
    var node = this.root;
    var indexCount = 0;
    var tempNode;
    var newNode = new Node(value);
    var  tempNodePrev;

    while (node.next !== null) {
        if (indexCount === index - 1) {
            tempNodePrev = node;
        }
        if (indexCount === index) {
            tempNode = node;
            tempNodePrev.next = newNode;
            tempNodePrev.next.next = tempNode;
        }
        indexCount++;
        node = node.next;
    }
    return this.root;
};

// LinkedList.prototype.sort = function () {
//     var node = this.root;
//     var j = this.root.next;
//     var temp;
//     var tempSecond;
//     var result = new LinkedList();
//     while (node !== null) {
//         if (node.value >  node.next.value) {
//             result.push(node.next);
//             result.push(node.next);
//             node = node.next;
//         }
//         else {
//             result.push(node);
//         }
//         node = node.next;
//     }
//         return result;
// };

// LinkedList.prototype.sort = function () {
//
//     var swapped;
//     var i;
//     // var node = this.root;
//     // var tempNode1 = new Node();
//     // var tempNode2 = new Node();
//     var ptr1;
//     var lptr = null;
//
//     /* Checking for empty list */
//     if (this.root === null)
//         return;
//     do {
//         swapped = 0;
//         ptr1 = this.root;
//         while (ptr1.next !== lptr) {
//             if (ptr1.data > ptr1.next.data) {
//                 this.swap(ptr1, ptr1.next);
//                 swapped = 1;
//             }
//             ptr1 = ptr1.next;
//         }
//         lptr = ptr1;
//     }
//     while (swapped);
// };

//
// LinkedList.prototype.swap = function (nodeOne, nodeTwo) {
//     var temp = nodeOne.value;
//     nodeOne.value = nodeTwo.value;
//     nodeTwo.value = temp;
// };

LinkedList.prototype.swap = function(ptr1, ptr2)
{
    var tmp = ptr2.next;
    ptr2.next = ptr1;
    ptr1.next = tmp;
    return ptr2;
};

/* Function to sort the list */
// LinkedList.prototype.sort = function () {
//     var count = this.getSize();
//     var h = new LinkedList();
//
//     for (i = 0; i <= count; i++) {
//         h = this;
//         console.log(h)
//         var swapped = 0;
//
//         for (j = 0; j < count - i - 1; j++) {
//
//             var p1 = h;
//             var p2 = p1.next;
//
//             if (p1.value > p2.value) {
//
//                 /* update the link after swapping */
//             h = this.swap(p1, p2);
//                 swapped = 1;
//             }
//
//             h = h.next;
//         }
//
//         /* break if the loop ended without any swap */
//         if (swapped === 0)
//             break;
//     }
// };

var linkNode = new LinkedList();

var l = new LinkedList();
l.init([95,85,77,65,35,22]);
l.sort();

module.exports = {
    res,
    linkNode
};