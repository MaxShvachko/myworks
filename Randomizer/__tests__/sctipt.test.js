const random = require("../script");
const jsdom = require("jsdom");

const { JSDOM } = jsdom;
const dom = new JSDOM(`<!DOCTYPE html><html><head><body></body></head></html>`);

checkForEndNumbers = random.checkForEndNumbers;
randomNumber = random.randomNumber;
checkNotNaN = random.checkNotNaN;
generateRandom = random.generateRandom;
checkForUsedNumber = random.checkForUsedNumber;

describe('checkForEndNumbers', () => {
  test('checkForEndNumbers in array (if 3 then true)', () => {
    expect(checkForEndNumbers(5 ,[1, 3, 4, 5, 2])).toBe(true);
  });
  test('checkForEndNumbers in array (if 6 then false)', () => {
    expect(checkForEndNumbers(6 ,[1, 3, 4, 5, 2])).toBe(false);
  });
});

test('randomNumber', () => {
  expect(randomNumber(1, 2)).toBe(1);
});

// describe('checkNotNaN ', () => {
//   var min = dom.window.document.createElement("input");
//   min.id = "minValue";
//   var max = dom.window.document.createElement("input");
//   max.id = "maxValue";
//   var output = dom.window.document.createElement("input");
//   output.id = "outputF";
//   var generate = dom.window.document.createElement("button");
//
//   output.id = "outputF";
//   var result = NaN;
//   dom.window.document.body.append(min);
//   dom.window.document.body.append(max);
//   dom.window.document.body.append(output);
//   // dom.window.document.body.append(generate);
//
//   min.value = "abc";
//   max.value = "5";
//   output.value = "";
//   output.value = "Pres Reset";
//
//   var expectMin = "Enter a number";
//   var expectMax = "Enter a number";
//   // var expectDisabledButton = generate.disabled = true;
//   var expectOutput = "Pres Reset";
//
//
//   test('checkNotNaN result is a NaN', () => {
//     expect(checkNotNaN(min.value, max.value, result)).toBe(expectMin, expectMax, expectOutput);
//   });
// });

describe('generateRandom  ', () => {
  var min = dom.window.document.createElement("input");
  min.id = 'minValue';
  var max = dom.window.document.createElement("input");
  max.id = 'maxValue';
  var output = dom.window.document.createElement("input").id = ('outputValue');

  dom.window.document.body.append(min);
  dom.window.document.body.append(max);
  dom.window.document.body.append(output);

  test('generateRandom min bigger then max', () => {
    min.value = "5";
    max.value = "3";
    var actual = generateRandom(min, max);
    var expectOutput = min.value = "Min must be less then max";
    expect(actual).toBe(expectOutput);
  });

  // test('generateRandom Enter positive number', () => {
  //   min.value = "-5";
  //   max.value = "4";
  //   var actual = generateRandom(min, max);
  //   var expectOutput = output.value = "Enter positive number";
  //   expect(actual).toBe(expectOutput);
  // });
  test('checkForUsedNumber Enter positive number', () => {
    minValue = "1";
    maxValue = "1";
    var randomArray = [1];
    var actual = checkForUsedNumber(min, max, randomArray);
    var expectOutput = output.value = "All numbers used";
    var disabledValue = true;
    expect(actual).toBe(expectOutput, disabledValue);
  });
});

