var randomArray = [];
var result;
var min = document.getElementById("minValue");
var max = document.getElementById("maxValue");
var output = document.getElementById("outputValue");
var generate = document.getElementById("generate");

var reset = document.getElementById("reset");

window.onload = function () {
  generate.addEventListener('click', generateRandom);
  reset.addEventListener('click', resetItems);
};

function checkForUsedNumber(min, max, randomArray) {
  var minValue = parseInt(min.value);
  var maxValue = parseInt(max.value) + 1;
  if (randomArray.length === (maxValue - minValue)) {
    output.value = "All numbers used";
    generate.disabled = true;
  }
}

function generateRandom() {
  var minValue = parseInt(min.value);
  var maxValue = parseInt(max.value) + 1;

  if (minValue < 0 || maxValue < 0) {
    return output.value = "Enter positive number";
  } else if (maxValue < minValue) {
    return min.value = "Min must be less then max";
  }

  checkForUsedNumber(min, max, randomArray);

  result = randomNumber(minValue, maxValue);

  if (checkForEndNumbers(result, randomArray)) {
    generateRandom(minValue, maxValue);
  } else {
    randomArray.push(result);
    output.value = result;
  }

  checkNotNaN(min, max, result);
  console.log(result, randomArray, typeof minValue);
}

function randomNumber(minValue, maxValue) {
  return Math.floor(Math.random() * (maxValue - minValue)) + minValue;
}

function checkForEndNumbers(result, randomArray) {
  for (var i = 0; i < randomArray.length; i++) {
    if (result === randomArray[i]) {
      return true;
    }
  }
  return false;
}

function resetItems() {
  randomArray = [];
  if (generate.disabled = true) {
    generate.disabled = false;
  }
  min.value = "";
  max.value = "";
  output.value = "";
}

function checkNotNaN(min, max, result) {
  if (Number.isNaN(result)) {
    min.value = "Enter a number";
    max.value = "Enter a number";
    output.value = "Pres Reset";
  }
}

module.exports = {
  checkNotNaN: checkNotNaN,
  checkForEndNumbers: checkForEndNumbers,
  randomNumber: randomNumber,
  generateRandom: generateRandom,
  checkForUsedNumber: checkForUsedNumber
};




