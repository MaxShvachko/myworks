var convert = require('../src/index');

var jsdom = require("jsdom");

var {JSDOM} = jsdom;
var dom = new JSDOM(`<!DOCTYPE html><html lang="en"><head></head><body></body></html>`);

global.window = window;
global.document = window.document;

describe('toggleModal toggle class show for modal', () => {
    var modalDiv = dom.window.document.createElement("div");
    var blurDiv = dom.window.document.createElement("div");

    dom.window.document.body.append(modalDiv);
    dom.window.document.body.append(blurDiv);

    convert.toggleModal(modalDiv, blurDiv);
    test('toggleModal is', () => {
        var expected = modalDiv.className = ("settings-container--show");
        expect("settings-container--show").toBe(expected);
    });
    test('toggleModal is', () => {
        var expected = blurDiv.className = ("container--action");
        expect("container--action").toBe(expected);
    });
});

describe('checkForVocabulary check libruary', () => {
    const vocabulary = {
        "english": [
            "Change language",
            "Accept",
            "Converter",
            "Input",
            "Meters",
            "Versts",
            "Yards",
            "Miles",
            "Foots",
            "Convert",
            "Output",
            "Meters",
            "Versts",
            "Yards",
            "Miles",
            "Foots",
        ],
        "russian": [
            "Изменить язык",
            "Применить",
            "Конвернтер",
            "Ввод",
            "Метры",
            "Версты",
            "Ярды",
            "Мили",
            "Футы",
            "Конвертировать",
            "Вывод",
            "Метры",
            "Версты",
            "Ярды",
            "Мили",
            "Футы",
        ],
        "arabian": [
            "اللغة",
            "للتقديم",
            "محول",
            "دخول",
            "متر",
            "معالم",
            "ياردة",
            "ميل",
            "قدم",
            "تحويل",
            "استنتاج",
            "متر",
            "معالم",
            "ياردة",
            "ميل",
            "قدم"
        ]
    };

    let container = dom.window.document.createElement("div");
    container.classList.add("main-container");

    dom.window.document.body.append(container);
    test('checkForVocabulary russian', () => {
        let expected = vocabulary.russian;

        let actual = convert.checkForVocabulary("Русский", container);

        expect(actual).toEqual(expected);
    });
    test('checkForVocabulary English', () => {
        let expected = vocabulary.english;

        let actual = convert.checkForVocabulary("English", container);

        expect(actual).toEqual(expected);
    });
    test('toggleModal is', () => {
        let expected = vocabulary.arabian;

        let actual = convert.checkForVocabulary("Arabian", container);

        expect(actual).toEqual(expected);
    });
});

describe('getConvertValue check libruary', () => {
    let output = dom.window.document.createElement("output");

    let select1 = dom.window.document.createElement("select");
    let select2 = dom.window.document.createElement("select");
    let option1 =  dom.window.document.createElement("option");
    let option2 =  dom.window.document.createElement("option");
    select1.append(option1);
    select2.append(option2);

    dom.window.document.body.append(select1);
    dom.window.document.body.append(select2);
    dom.window.document.body.append(output);

    test('getConvertValue meters to', () => {
        option1.value = "2";
        option2.value = "5";

        convert.secondConvert(option1.value, option2.value);

        let actual = convert.getConvertValue(output);
        let expected = 25;
        expect(actual).toEqual(expected);
    });
    test('checkForVocabulary English', () => {
        let expected = vocabulary.english;

        let actual = convert.checkForVocabulary("English", container);

        expect(actual).toEqual(expected);
    });
    test('toggleModal is', () => {
        let expected = vocabulary.arabian;

        let actual = convert.checkForVocabulary("Arabian", container);

        expect(actual).toEqual(expected);
    });
});
