import './less/index.less';
require('svg-url-loader!../assets/big-cogwheel.svg');

const initialValue = document.getElementById("initialValue");
const output = document.getElementById("output");
const convert = document.getElementById("convert");
const select1 = document.getElementById("selectFrom");
const select2 = document.getElementById("selectTo");
const accept = document.getElementById("accept");
const selectLang = document.getElementById("lang");
const container = document.getElementById("mainContainer");
const openModal = document.getElementById("openModal");
const closeModal = document.getElementById("closeModal");
const modal = document.getElementById("modal");
const blurModal = document.getElementById("blurModal");

initialValue.oninput = function () {
  convert.disabled = this.value.length <= 0;
  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
};

openModal.addEventListener("click", () => {toggleModal(modal, blurModal)} );
closeModal.addEventListener("click", () => {toggleModal(modal, blurModal)});
accept.addEventListener("click", changeLanguage);
convert.addEventListener("click", () => getConvertValue(output));

const metersObj = {
  "meter": 1,
  "yards": 0.9144,
  "mile": 1609.34,
  "foot": 0.3048,
  "verst": 1066.8
};

const vocabulary = {
  "english": [
    "Change language",
    "Accept",
    "Converter",
    "Input",
    "Meters",
    "Versts",
    "Yards",
    "Miles",
    "Foots",
    "Convert",
    "Output",
    "Meters",
    "Versts",
    "Yards",
    "Miles",
    "Foots",
  ],
  "russian": [
    "Изменить язык",
    "Применить",
    "Конвернтер",
    "Ввод",
    "Метры",
    "Версты",
    "Ярды",
    "Мили",
    "Футы",
    "Конвертировать",
    "Вывод",
    "Метры",
    "Версты",
    "Ярды",
    "Мили",
    "Футы",
  ],
  "arabian": [
    "اللغة",
    "للتقديم",
    "محول",
    "دخول",
      "متر",
    "معالم",
    "ياردة",
    "ميل",
    "قدم",
    "تحويل",
    "استنتاج",
      "متر",
    "معالم",
    "ياردة",
    "ميل",
    "قدم"
  ]
};

//Modal Section

function toggleModal(modalName, blurName) {
  modalName.classList.toggle("settings-container--show");
  blurName.classList.toggle("container--action");
}

//Change modal section
function changeLanguage() {
  changeTextPage(checkForVocabulary(currentLang(), container));
}

window.onload = function () {
  convert.disabled = true;
  if (localStorage.length !== 0) {
    changeTextPage(checkForVocabulary(localStorage.local, container));
  } else {
    changeTextPage(checkForVocabulary("English", container));
  }
};

function checkForVocabulary(stringLang, div) {
  let result;
  switch (stringLang) {
    case "English":
      result = vocabulary.english;
      div.classList.remove("main-container");
      break;
    case "Русский":
      result = vocabulary.russian;
      div.classList.remove("main-container");
      break;
    default :
      result = vocabulary.arabian;
      div.classList.add("main-container") ;
      break;
  }
  return result;
}

function currentLang() {
  var i = selectLang.selectedIndex;
  let lang = selectLang.options[i].text;
  localStorage.setItem('local', lang);
  return lang;
}

function changeTextPage(langLibrery) {
  let langLocal = document.getElementsByClassName("local");
  let counterLang = 0;

  for (let key in langLocal) {
    if (langLocal.hasOwnProperty(key)) {
      langLocal[key].innerText = langLibrery[counterLang];
    }
    counterLang++;
  }
}

//Convert section
function getConvertValue(output) {
  let options1 = select1.options[select1.selectedIndex].value;
  let options2 = select2.options[select2.selectedIndex].value;
  output.value = secondConvert(options1, options2);
}

function firstConvert(options1) {
  let inputValue = Number(initialValue.value);

  if (inputValue === false) {
    return output.value = "Enter a long";
  }
  return inputValue * metersObj[options1];
}

function secondConvert(options1, options2) {
  let convertFirst = firstConvert(options1);
  return convertFirst / metersObj[options2];
}

// module.exports = {
//   toggleModal: toggleModal,
//   checkForVocabulary: checkForVocabulary,
//   getConvertValue: getConvertValue,
//   secondConvert: secondConvert,
// };