import "./style/index.less"

const studentName = document.getElementById('studentName');
const studentMark = document.getElementById('studentMark');
const studentAge = document.getElementById('studentAge');
const studentSkills = document.getElementById('studentSkills');
const studentEnglish = document.getElementById('studentEnglish');

const generateAllStudents = document.getElementById('generateAllStudents');
const addStudent = document.getElementById('addStudent');
const innitAllStudents = document.getElementById('innitAllStudents');
const startRace = document.getElementById('startRace');
const canvas = document.getElementById('canvasField');
const ctx = canvas.getContext("2d");

const generateStudentName = document.getElementById('generateStudentName');
generateStudentName.addEventListener('click', () => {
  studentName.value = randomArrayItem(studentsList)
});
const generateStudentMark = document.getElementById('generateStudentMark');
generateStudentMark.addEventListener('click', () => {
  studentMark.value = randomData(1, 5)
});
const generateStudentAge = document.getElementById('generateStudentAge');
generateStudentAge.addEventListener('click', () => {
  studentAge.value = randomData(18, 35)
});
const generateStudentSkills = document.getElementById('generateStudentSkills');
generateStudentSkills.addEventListener('click', () => {
  studentSkills.value = randomSkills(arraySkills)
});
const generateStudentEnglish = document.getElementById('generateStudentEnglish');
generateStudentEnglish.addEventListener('click', () => {
  studentEnglish.value = randomArrayItem(arrayEnglishLevel)
});

const clear = document.querySelector('#clear');
clear.addEventListener('click', () => {
  arrayOfStudents.length = 0;
  ctx.clearRect(0, 0, canvas.width, canvas.height)
});

generateAllStudents.addEventListener('click', () => {
  studentName.value = randomArrayItem(studentsList);
  studentMark.value = randomData(2, 5);
  studentAge.value = randomData(18, 35);
  studentSkills.value = randomSkills(arraySkills);
  studentEnglish.value = randomArrayItem(arrayEnglishLevel);
});

startRace.addEventListener('click', () => race(arrayOfStudents));
innitAllStudents.addEventListener('click', () => init(arrayOfStudents));
addStudent.addEventListener('click', CreateNewStudent);

const url = 'https://randomuser.me/api/?results=25';
let arrayOfStudents = [];
let studentsList;
let arrayEnglishLevel = ['A0', 'A1', 'A2', 'B1', 'B2', 'C1'];
const arraySkills = ['js', 'react', 'wordpress'];
let stepPositionOfStudentX = 70;

function randomDelay(min, max) {
  let gradeTime = 1000;
  return Math.floor(Math.random() * (max - min) + min) * gradeTime;
}

function randomArrayItem(array) {
  let max = array.length - 1;
  let min = 0;
  let randomItem = Math.floor(Math.random() * (max - min) + min);
  return array[randomItem];
}

function randomData(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

function randomSkills(array) {
  let percent = Math.floor(Math.random() * (100 - 1) + 1);
  if (percent < 80) {
    return array
  } else {
    return randomArrayItem(array)
  }
}

fetch(url)
  .then((resp) => resp.json())
  .then(function (data) {
    studentsList = data.results;
    makeNormalNameToArray(studentsList);
    studentsList = makeNormalNameToArray(studentsList);
    return studentsList
  })
  .catch(function (error) {
    console.log(error);
  });

function makeNormalNameToArray(data) {
  let result = [];

  data.forEach(element => {
    for (var key in element) {
      if (key === "name") {
        let name = '';
        for (let i in element[key]) {
          if (i === 'first' || i === 'last') {
            name = name + element[key][i] + " ";
          }
        }
        name = name.slice(0, -1);
        result.push(name)
      }
    }
  });
  return result;
}

function studentPosition(array) {
  let arrayLength = array.length;
  let result;
  switch (arrayLength) {
    case 0:
      result = 0;
      break;
    case 1:
      result = 160;
      break;
    case 2:
      result = 320;
      break;
    case 3:
      result = 480;
      break;
    case 4:
      result = 600;
      break;
    default:
      result = 760;
      break;
  }
  return result;
}

function CreateNewStudent() {
  let studentObject = {
    name: studentName.value,
    mark: Number(studentMark.value),
    age: Number(studentAge.value),
    skills: getSkillsDataToArray(studentSkills),
    english: studentEnglish.value,
    x: 60,
    y: 70 + studentPosition(arrayOfStudents),
    r: 25,
  };
  arrayOfStudents.push(studentObject);
  console.log(arrayOfStudents);
}

function draw(progress, x, y, r, color, text = false) {
  ctx.clearRect(x - r - 1, y - r - 1, 2 * (r + 1), 2 * (r + 1));

  let current = progress * 2 * Math.PI;
  ctx.beginPath();
  ctx.arc(x, y, r, 0, current);
  ctx.strokeStyle = color;
  ctx.stroke();
  if (text) ctx.fillText(text, x - r, y);
  ctx.closePath();
}

function drawCircle(duration, x, y, r, color, text) {
  const start = performance.now();
  let progress;

  const timer = setInterval(() => {
    if (progress > 1) {
      clearInterval(timer);
      return;
    }
    let now = performance.now();
    progress = (now - start) / duration;
    draw(progress, x, y, r, color, text);
  }, 10);
  return timer;
}

function init(array) {
  let canvas = document.getElementById('canvasField');
  let ctx = canvas.getContext("2d");
  array.forEach(element => {
    drawCircle(3000, element.x, element.y, element.r, "green", element.name);
  })
}

function getSkillsDataToArray(inputElement) {
  return inputElement.value.toLowerCase().replace(/ /g, "").split(",");
}

function getCertificate(student) {
  return new Promise((resolve, reject) => {
    let delayFiveToTen = randomDelay(5, 10);
    student.x = student.x + stepPositionOfStudentX;
    const timer = drawCircle(delayFiveToTen, student.x * 2, student.y, student.r, "purple", "JS Course");
    setTimeout(() => {
      if (student.mark >= 4) {
        console.log(`Mark ${student.name} ${student.mark}`);
        clearInterval(timer);
        draw(1, student.x * 2, student.y, student.r, "green", "JS Course");
        student.x = student.x + stepPositionOfStudentX;
        resolve(student)
      } else {
        console.log(student.name + " Low mark");
        clearInterval(timer);
        draw(1, student.x * 2, student.y, student.r, "red", "JS Course");

      }
    }, delayFiveToTen)
  })
}

function goToHrInterview(student) {
  return new Promise((resolve, reject) => {
    let delayOneToTree = randomDelay(1, 3);
    const timer = drawCircle(delayOneToTree, student.x * 3, student.y - 2 * student.r - 5, student.r, "purple", "HR");
    setTimeout(() => {
      if (student.age > 21 && student.age < 30) {
        console.log(`Age ${student.age}`);
        clearInterval(timer);
        draw(1, student.x * 3, student.y - 2 * student.r - 5, student.r, "green", "HR");
        resolve(student)
      } else {
        console.log(`${student.name} Age false`);
        clearInterval(timer);
        draw(1, student.x * 3, student.y - 2 * student.r - 5, student.r, "red", "HR");
      }
    }, delayOneToTree)
  })
}

function goToProjectInterview(student) {
  return new Promise((resolve, reject) => {
    let delayTenToTwenty = randomDelay(10, 20);
    const timer = drawCircle(delayTenToTwenty, student.x * 3, student.y, student.r, "purple", "Project");
    setTimeout(() => {
      if (student.skills.length === 3) {
        console.log(student.name + " passed Project interview");
        clearInterval(timer);
        draw(1, student.x * 3, student.y, student.r, "green", "Project");
        resolve(student)
      } else {
        console.log(`${student.name} Skills not enough`);
        clearInterval(timer);
        draw(1, student.x * 3, student.y, student.r, "red", "Project");
      }
    }, delayTenToTwenty);
  })
}

function goToClientInterview(student) {
  return new Promise((resolve, reject) => {
    let delayFiveToTen = randomDelay(5, 10);
    const timer = drawCircle(delayFiveToTen, student.x * 3, student.y + 2 * student.r + 5, student.r, "purple", "Client");
    setTimeout(() => {
      if (student.english === "B1" || student.english === "B2" || student.english === "C1") {
        console.log(`Level english ${student.english}`);
        clearInterval(timer);
        draw(1, student.x * 3, student.y + 2 * student.r + 5, student.r, "green", "Client");
        resolve(student);
      } else {
        console.log(`Level english ${student.english} false`);
        clearInterval(timer);
        draw(1, student.x * 3, student.y + 2 * student.r + 5, student.r, "red", "Client");
      }
    }, delayFiveToTen);
  })
}

function getJobOffer(student) {
  return Promise.all([goToHrInterview(student), goToProjectInterview(student), goToClientInterview(student)])
    .then(() => {
      return new Promise(((resolve, reject) => {
        if (student) {
          resolve(student);
        }
      }))
    })
    .catch(student => {
      console.log(`${student.name} lost`)
    })
}

function goJsCourse(student) {
  return new Promise((resolve, reject) => {
    if (student) {
      resolve(student);
    } else {
      reject(student);
    }
  })
}

function race(students) {
  const studentsArray = [];
  students.forEach(el => {
    studentsArray.push(
      goJsCourse(el).then(getCertificate).then(getJobOffer)
    )
  });
  Promise.race(studentsArray)
    .then(student => alert(`${student.name} get offer`))
    .catch(error => console.error(error));
}
