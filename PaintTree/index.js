var Node = function(x,y,r, ctx, data) {
    this.leftNode = null;
    this.rightNode = null;

    this.draw = function() {
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2*Math.PI);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeText(data, x, y);
    };

    this.getData = function() { return data; };
    this.getX = function() { return x; };
    this.getY = function() { return y; };
    this.getRadius = function() { return r; };

    this.leftCoordinate = function() {
        return {cx: (x - (3*r)), cy: (y + (3*r))}
    };

    this.rightCoordinate = function() {
        return {cx: (x + (3*r)), cy: (y+(3*r))}
    };
};

var Line = function() {
    this.draw = function(x, y, toX, toY, r, ctx) {
        var moveToX = x;
        var moveToY = y + r;
        var lineToX = toX;
        var lineToY = toY - r;
        ctx.beginPath();
        ctx.moveTo(moveToX, moveToY);
        ctx.lineTo(lineToX, lineToY);
        ctx.stroke();
    };
};

var BTree = function() {
    var c = document.getElementById('my-canvas');
    var ctx = c.getContext('2d');
    var line = new Line();
    this.root = null;

    var self = this;

    this.getRoot = function() { return this.root; };

    this.add = function( data) {

        if(this.root) {
            this.recursiveAddNode(this.root, null, null, data);
        } else {
            this.root = this.addAndDisplayNode(200, 20, 15, ctx, data);
            return;
        }
    };

    this.recursiveAddNode = function(node, prevNode, coordinateCallback, data) {
        if(!node) {
            var xy = coordinateCallback();
            var newNode = this.addAndDisplayNode(xy.cx, xy.cy, 15, ctx, data);
            line.draw(prevNode.getX(), prevNode.getY(), xy.cx, xy.cy, prevNode.getRadius(), ctx);
            return newNode;
        }
        else {
            if(data <= node.getData()) {
                node.left = this.recursiveAddNode(node.left, node, node.leftCoordinate, data);
            }
            else {
                node.right = this.recursiveAddNode(node.right, node, node.rightCoordinate, data);
            }
            return node;
        }
    };

    this.addAndDisplayNode = function(x, y, r, ctx, data) {
        var node = new Node(x, y, r, ctx, data);
        node.draw();
        return node;
    };
};

var addToTree = function() {
    input = document.getElementById('tree-input');
    value = parseInt(input.value);
    if(value)
        btree.add(value);
    else
        alert("Wrong input");
};

var btree = new BTree();