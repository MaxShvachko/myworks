import React, { Component } from "react";
import "./style/index.less";
import {Input} from "./modules/Input"
import {CustomButton} from "./modules/CustomButton"

class TimerWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seconds: 0,
      minutes: 0,
      minSeconds: "0",
      timer: null,
      isOn: false,
      inputValue: "",
    };
  }

  startTimer = () => {
    if (this.state.isOn === true || this.state.seconds === 0 && this.state.minutes === 0) {
      return
    }
    this.timer = setInterval( () => {
      this.setState({isOn: true});
      const { seconds, minutes} = this.state;

      if (seconds > 0) {
      this.setState(({seconds}) => ({
        seconds: seconds - 1
      }))
    }
    if (seconds === 0) {
      if (minutes === 0) {
        clearInterval(this.timer);
      }else {
        if (seconds < 10) {
          this.setState({minSeconds: "0"});
        }
        this.setState({minSeconds: ""});
        this.setState(({minutes}) => ({
            minutes: minutes - 1,
            seconds: 59
          })
        )
      }
    }
    }, 1000);
  };

  setTimer = () => {
    this.setState({
      minutes: Math.floor(input.value / 60),
      seconds: input.value % 60,
      minSeconds: input.value % 60 > 10 ? "" : "0"
    });
  };

  stop = () => {
   this.setState({isOn: false});
    clearInterval(this.timer);
  };

  reset = () => {
    clearInterval(this.timer);
    this.setState({
      seconds: 0,
      minutes: 0,
      timeSec: 0,
      timer: null,
      isOn: false,
    });
    input.value = "";
  };

  render() {
    const {seconds, minutes, minSeconds} = this.state;
    return (
      <div className="main">
        <div className="main__container">
          <div className="timer">
            <Input inputClassName={"input-item"} idInput={"input"} value = {this.state.inputValue} callback= {(e) => this.setState({inputValue: e.target.value.replace(/\D/,'')})}  text={"Enter number"}/>
            <CustomButton buttonClassName={"button-item"} callback={this.setTimer} text={"set"}/>
            <CustomButton  buttonClassName={"button-item"} callback={this.startTimer} text={"start"}/>
            <CustomButton buttonClassName={"button-item"} callback={this.stop} text={"stop"}/>
            <CustomButton buttonClassName={"button-item"} callback={this.reset} text={"reset"}/>
          </div>
          <div className="display">{minutes}:{minSeconds}{seconds}</div>
      </div>
      </div>
    );
  }
}

export {TimerWrapper};