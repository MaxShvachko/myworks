import React from "react";

function CustomButton (props) {
  const {buttonClassName, callback, text} = props;
  return (
    <button className={buttonClassName} type={"button"} onClick={callback}>{text}</button>
  );
}

export {CustomButton};