import React from "react";

function Input (props){
  const {inputClassName, text, idInput, callback} = props;

  return (
    <div className={"input-container"}>
      <label htmlFor={idInput}>{text}</label>
      <input className={inputClassName} id={idInput} onChange={callback}  placeholder={text}/>
    </div>

  )
}

export {Input};