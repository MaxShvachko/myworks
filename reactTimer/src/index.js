import React from "react";
import ReactDOM from "react-dom";
import {TimerWrapper} from "./App.js";

ReactDOM.render(<TimerWrapper />, document.getElementById("root"));
