import "./style/index.less"
import drawTree from "canvas-binary-tree";

var btnInit = document.getElementById("btn");
var input = document.getElementById("input");
var canvas = document.getElementById("main-canvas");

btnInit.addEventListener("click", init);

function BSTree() {
  this.root = null;
}

var Node = function(value) {
  this.value = value;
  this.right = null;
  this.left= null;
};

BSTree.prototype.init = function (array) {
  this.clear();
  for (var i = 0; i < array.length; i++) {
    this.add(array[i]);
  }
};

BSTree.prototype.clear = function (array) {
  this.root = null;
};

BSTree.prototype.add = function(value) {
  if (value === null) {
    return;
  }
  this.root = this.addNode(this.root, value);
};

BSTree.prototype.addNode = function(node, value) {
  if (node === null) {
    node = new Node(value);
  }
  else if (value < node.value) {
    node.left = this.addNode(node.left, value);
  } else {
    node.right = this.addNode(node.right, value);
  }
  return node;
};

BSTree.prototype.traverse = function() {
  this.root.visit()
};

BSTree.prototype.find = function(value) {
  return this.root.find(value)
};

Node.prototype.find = function (value) {
  if (this.value === value ) {
    return this;
  }else if (this.value > value && this.left !== null) {
    return  this.left.find(value);
  }else if (this.value < value && this.right !== null){
    return  this.right.find(value);
  } else {
    return null;
  }
};

Node.prototype.visit = function() {
  if(this.left !== null) {
    this.left.visit();
  }
  console.log(this.value);
  if(this.right !== null) {
    this.right.visit();
  }
};

function init() {
  var initArray = input.value.split(" ");
  var initNumber = initArray.map((element) => Number(element));
  var tree = new BSTree();
  tree.init(initNumber);
  tree.traverse();
  drawTree(canvas, tree)
}