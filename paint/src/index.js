import "./style/index.less"

var localhost = "http://localhost:3000/";
var heroku = 'https://paintformylitteldoter.herokuapp.com/';
var socket = io.connect(localhost);

let canvas = document.getElementById("main-canvas");
let ctx = canvas.getContext('2d');
let inputColor = document.getElementById("inputColor");
let inputRange = document.getElementById("inputRange");
let xValue = document.getElementById("xValue");
let yValue = document.getElementById("yValue");
let clear = document.getElementById('clearBtn');

let isDraw = false;

let objLine = {
  color: "black",
  width: 10,
  moveToArray: [],
  line: []
};

let arrayAllLines = [];

ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 10;

var clearCanvas = () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  arrayAllLines = [];
};
clear.addEventListener('click', clearCanvas);

//Massage dom
let massage = document.getElementById('massage');
let handle = document.getElementById('handle');
let btn = document.getElementById('send');
let output = document.getElementById('output');
//chat

let get = document.getElementById('get');

btn.addEventListener('click', function () {
  socket.emit('chat', {
    handel: handle.value,
    massage: massage.value,
  });
});

socket.on('chat', function (data) {
  output.innerHTML += "<p>" + data.handel + " :" + data.massage + "</p>"
});

socket.on('line', function (data) {
  let dataValue = JSON.parse(data);
  parallelDrawing(dataValue)

});

inputColor.addEventListener("change", getColor);
inputRange.addEventListener("change", (e) => {
  ctx.lineWidth = e.target.value;
});

function getColor() {
  ctx.strokeStyle = inputColor.value;
}

canvas.onmousedown = (e) => {
  ctx.beginPath();
  ctx.moveTo(e.offsetX, e.offsetY);
  objLine.moveToArray = [e.offsetX, e.offsetY];
  isDraw = true;
};

canvas.onmouseup = (e) => {
  objLine.color = ctx.strokeStyle;
  objLine.width = ctx.lineWidth;
  arrayAllLines.push(objLine);
  socket.emit('line', JSON.stringify(arrayAllLines));
  objLine.line.length = 0;
};

canvas.addEventListener("mouseout", () => {
  isDraw = false;
  yValue.innerText = `0 px`;
  xValue.innerText = `0 px`;
});

canvas.onmousemove = (e) => {
  let x = e.offsetX;
  let xValue = document.getElementById("xValue");
  xValue.innerText = `${x} px`;
  let y = e.offsetY;
  let yValue = document.getElementById("yValue");
  yValue.innerText = `${y} px`;
  if (e.buttons === 1 && isDraw) {
    writeDotsToArray(x, y, objLine.line);
    ctx.lineTo(x, y);
    ctx.stroke();
  }
};

function writeDotsToArray(xArray, yArray, array) {
  array.push(xArray);
  array.push(yArray);
}

function parallelDrawing(arrayData) {
  for (let i = 0; i < arrayData.length; i++) {
    ctx.beginPath();
    ctx.moveTo(+(arrayData[i].moveToArray[i]), +(arrayData[i].moveToArray[i + 1]));

    ctx.strokeStyle = arrayData[i].color;
    ctx.lineWidth = +(arrayData[i].lineWidth);
    var lineArr = arrayData[i].line;
    if (lineArr === undefined) return;
    for (var j = 2; j < lineArr.length; j = j + 2) {
      if (lineArr[j + 1] === undefined) {
        return;
      } else {
        ctx.lineTo(+(lineArr[j]), +(lineArr[j + 1]));
        ctx.stroke()
      }
    }
  }
}




























