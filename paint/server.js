var express = require('express');
var app = express();
var path = require('path');
const bodyParser = require("body-parser");
var socket = require('socket.io');
app.use(express.static(path.join(__dirname, 'dist')));
const PORT = process.env.PORT || 3000;


const server = app.listen(PORT, function () {
  console.log("connection");
});

var io = socket(server);

io.on('connection', function (socket) {
  console.log("made socket connection", socket.id);

  socket.on('chat', function (data) {

    io.sockets.emit('chat', data);
    console.log(data)
  });

  socket.on('line', function (data) {
    socket.broadcast.emit('line', data);
    io.sockets.emit('line', data);
  });
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


