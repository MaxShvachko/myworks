
var jsdom = require("jsdom");

var {JSDOM} = jsdom
var dom = new JSDOM(`<!DOCTYPE html><html lang="en"><head></head><body></body></html>`);

module.exports = {
    jsdom: jsdom,
    JSDOM: {JSDOM} = jsdom,
    dom: dom
};