var calc = require("../client/script/index.js");

var jsdom = require('jsdom');

var {JSDOM} = jsdom;

const dom = new JSDOM(`<!DOCTYPE html><html lang="en"><head></head><body></body></html>`);
global.window = dom.window;
global.document = dom.window.document;

describe('displayValue check displayValue', () => {
  var btn7 = dom.window.document.createElement("input");
  var display = dom.window.document.createElement("input");

  dom.window.document.body.append(btn7);
  dom.window.document.body.append(display);

  test('displayValue is', () => {
    btn7.value = "2";
    display.value = "1";
    var expected = "12";

    calc.displayValue(display, btn7.value);
    expect(display.value).toBe(expected);
  });
});

describe('clearDisplay', () => {
  var display = dom.window.document.createElement("input");

  dom.window.document.body.append(display);

  test('clearDisplay clean 123 to "" ', () => {
    display.value = "123";
    calc.calculator.firstOperand = "125";
    calc.calculator.secondOperand = "264";
    calc.calculator.operator = "/";
    calc.clearDisplay();
    var action = [display.value,
      calc.calculator.firstOperand,
      calc.calculator.secondOperand,
      calc.calculator.operator];
    var expected = ["", null, null, null];

    expect(action).toEqual(expected);
  });
});

describe('back delete last current number', () => {
  var display = dom.window.document.createElement("input");
  dom.window.document.body.append(display);

  test('back delete number 6 in number 256', () => {
    display.value = "256";
    calc.back(display);
    var expected = "25";
    expect(display.value).toBe(expected);
  });
});

describe('plus', () => {
  test('plus sum numbers 2 and 3', () => {
    expect(plus("2", "3")).toBe(5);
  });
  test('plus sum numbers -2 and 95', () => {
    expect(plus("-2", "95")).toBe(93);
  });
  test('minus of numbers 25.45 + 2.25 = )', () => {
    expect(plus("25.45", "2.25")).toBe(27.7);
  });
});

describe('minus', () => {
  test('minus of numbers 85 - 32 = )', () => {
    expect(minus("85", "32")).toBe(53);
  });
  test('minus of numbers -85 - -32 = )', () => {
    expect(minus("-85", "-32")).toBe(-53);
  });
  test('minus of numbers 25.45 - 2.24 = )', () => {
    expect(minus("25.45", "2.24")).toBe(23.21);
  });
});

describe('divide', () => {
  test('divide of numbers 100 / 25 = 4)', () => {
    expect(divide("100", "25")).toBe(4);
  });
  test('minus of numbers 25.45 / 2 = 12.725)', () => {
    expect(divide("25.45", "2")).toBe(12.725);
  });
});

describe('multiply', () => {
  test('multiply of numbers 100 * 25 = 2500)', () => {
    expect(multiply("100", "25")).toBe(2500);
  });
  test('minus of numbers 25.45 / 2 = 50.9)', () => {
    expect(multiply("25.45", "2")).toBe(50.9);
  });
})

describe('prep Prepare value to function equal', () => {
  var displayPre = dom.window.document.createElement("input");
  var display = dom.window.document.createElement("input");

  var btnPlus = dom.window.document.createElement("button");
  dom.window.document.body.append(displayPre);
  dom.window.document.body.append(display);
  dom.window.document.body.append(btnPlus);
  btnPlus.value = "+";

  test('prep Prepare value to function equal', () => {
    display.value = "143";
    prep(display, btnPlus, displayPre);
    var action = [calculator.operator,
      calculator.firstOperand,
      displayPre.value,
        display.value
      ];
    var expected = ["+",
      "143",
    "143+",
     ""];
    expect(action).toEqual(expected);
  });
});

describe('prep Prepare value to function equal', () => {
  var displayPre = dom.window.document.createElement("input");
  var display = dom.window.document.createElement("input");

  var btnPlus = dom.window.document.createElement("button");
  dom.window.document.body.append(displayPre);
  dom.window.document.body.append(display);
  dom.window.document.body.append(btnPlus);
  btnPlus.value = "+";

  test('prep Prepare value to function equal', () => {
    display.value = "143";
    prep(display, btnPlus, displayPre);
    var action = [calculator.operator,
      calculator.firstOperand,
      displayPre.value,
      display.value
    ];
    var expected = ["+",
      "143",
      "143+",
      ""];
    expect(action).toEqual(expected);
  });
});