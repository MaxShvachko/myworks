import '../style/style.less'

var displayPre = document.getElementById("displayPre");
var display = document.getElementById("display");

var btnEqually = document.getElementById("btnEqually");
var clearAllValue = document.getElementById("clearAllValue");
var backSpace = document.getElementById("backspace");

document.onkeydown = function (event) {

  if (event.key >= 0 && event.key < 10) {
    displayValue(display ,event.key)
  }
  if (event.key === '+' || event.key === '-' || event.key === '*' || event.key === '/') {
    prep(display, event);
  } else {
    switch (event.key) {
      case "Backspace": back();
        break;
      case ".": displayValue(display,".");
        break;
      case "Delete": clearDisplay();
        break;
      case "Enter": equal();
        break;
      default: return;
        break;
    }
  }
};

var calculator = {
  firstOperand: null,
  secondOperand: null,
  operator: null
};

clearAllValue.addEventListener('click', clearDisplay);
btnEqually.addEventListener('click', equal);
backSpace.addEventListener('click', back);

var num = document.getElementsByClassName("btn--number");


var operators = document.getElementsByClassName("btn--operator");

document.addEventListener("DOMContentLoaded", () => {
  for (var j = 0; j < num.length; j++) {
    let numValue = num[j].innerText;
    let callback = function (){displayValue(display, numValue)};
    num[j].addEventListener('click', callback, false);
  }

  for (var i = 0; i < operators.length; i++) {
    let btnValue = operators[i];
    operators[i].addEventListener('click', function (){prep(display, btnValue)}, false);
  }
} );

// arg displayPre to test
function prep(display, btnValue) {
  var operandValue;
  if (btnValue.value === undefined) {
    operandValue = btnValue.key;
  } else {
    operandValue = btnValue.value;
  }
  calculator.operator = operandValue;
  calculator.firstOperand = display.value;
  displayPre.value = calculator.firstOperand + calculator.operator;
  display.value = "";
}

function displayValue(display, value) {
  if (display.value.length > 25) return;
  if (value === "." && display.value.includes(".") || value === "0" && display.value[0] === "0"){
    if (value === "0" && display.value[0] === "0" && display.value.includes(".")) {
      display.value += value;
      return;
    }
    else {
      return
    }
  }
  if (display.value[0] === '0' && value > 0) {
    if (display.value[1] === '.') {
      display.value += value;
        return;
    }else {
      display.value = display.value[0];
      return;
    }
  }
  display.value += value;
}
//arg display to test
function clearDisplay() {
  display.value = "";
  displayPre.value = "";
  calculator.firstOperand = null;
  calculator.secondOperand = null;
  calculator.operator = null;
}
// arg display to test
function back() {
  display.value =  display.value.substring(0, display.value.length - 1);
}

// arg display to test
function equal() {
  displayPre.value = "";
  var operator = calculator.operator;
  var a = calculator.firstOperand;
  var b = calculator.secondOperand = display.value;
  switch (operator) {
    case "+":
      display.value = +a + +b;
      break;
    case "-":
      display.value = a - b;
      break;
    case "/":
      display.value = a / b;
      break;
    case "*":
      display.value = a * b;
      break;
    default: display.value;
      break;
  }
}

// module.exports = {displayValue, clearDisplay, back, calculator, prep, equal};




