import {BSTree, Node} from '../index';

describe('BSTree init', () => {
    var bTree = new BSTree();
    test('BSTree init is', () => {
        bTree.init([9,5,13,3,7,6,1,2,12,13,14]);
        var action = bTree.root;
        var expected = {
            "left": {
                "left": {
                    "left": {
                        "left": null,
                        "right": {
                            "left": null,
                            "right": null,
                            "value": 2
                        },
                        "value": 1
                    },
                    "right": null,
                    "value": 3
                },
                "right": {
                    "left": {
                        "left": null,
                        "right": null,
                        "value": 6
                    },
                    "right": null,
                    "value": 7
                },
                "value": 5
            },
            "right": {
                "left": {
                    "left": null,
                    "right": null,
                    "value": 12
                },
                "right": {
                    "left": null,
                    "right": {
                        "left": null,
                        "right": null,
                        "value": 14
                    },
                    "value": 13
                },
                "value": 13
            },
            "value": 9
        };
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree clear', () => {
    var bTree = new BSTree();
    test('BSTree clear is', () => {
        bTree.init([9,5,13,3,7,6,1,2,12,13,14]);
        bTree.clear();
        var action = bTree.root;
        var expected = null;
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree add', () => {
    var bTree = new BSTree();
    test('BSTree add is', () => {
        bTree.init([9,5]);
        bTree.add(7);
        var action = bTree.root;
        var expected = {
            "left": {
                "left": null,
                "right": {
                    "left": null,
                    "right": null,
                    "value": 7
                },
                "value": 5
            },
            "right": null,
            "value": 9
        };
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree addNode', () => {
    var bTree = new BSTree();
    test('BSTree addNode is', () => {
        bTree.init([9,5]);
        var action = bTree.addNode(bTree.root, 7);
        var expected = {
            "left": {
                "left": null,
                "right": {
                    "left": null,
                    "right": null,
                    "value": 7
                },
                "value": 5
            },
            "right": null,
            "value": 9
        };
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree getHeight', () => {
    var bTree = new BSTree();
    test('BSTree getHeight is', () => {
        bTree.init([9,5,2,9]);
        var action = bTree.getHeight();
        var expected = 3;
        expect(action).toBe(expected);
        bTree.clear();
    });
});

describe('BSTree height', () => {
    var bTree = new BSTree();
    test('BSTree height is', () => {
        bTree.init([9,5,2,9]);
        var action = bTree.height(bTree.root);
        var expected = 3;
        expect(action).toBe(expected);
        bTree.clear();
    });
});

describe('BSTree find', () => {
    var bTree = new BSTree();
    test('BSTree find is', () => {
        bTree.init([9,5,2,9]);
        var action = bTree.find(2);
        var expected = {
            "left": null,
            "right": null,
            "value": 2
        };
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree reverseSup', () => {
    var bTree = new BSTree();
    test('BSTree reverseSup is', () => {
        bTree.init([8,5,13,3,11,17]);
        bTree.reverseSup();
        var action = bTree.root;
        var expected = {
            "left": {
                "left": {
                    "left": null,
                    "right": null,
                    "value": 17
                },
                "right": {
                    "left": null,
                    "right": null,
                    "value": 11
                },
                "value": 13
            },
            "right": {
                "left": null,
                "right": {
                    "left": null,
                    "right": null,
                    "value": 3
                },
                "value": 5
            },
            "value": 8
        };
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree getNodes', () => {
    var bTree = new BSTree();
    test('BSTree getNodes is', () => {
        bTree.init([9, 17,8, 25,13,12]);
        var action = bTree.getNodes();
        var expected = 6;
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree getLeaves', () => {
    var bTree = new BSTree();
    test('BSTree getLeaves is', () => {
        bTree.init([9, 17,8, 25,13,12]);
        var action = bTree.getLeaves();
        var expected = 3;
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree nodes', () => {
    var bTree = new BSTree();
    test('BSTree nodes is', () => {
        bTree.init([9, 17,8, 25,13,12]);
        var action = bTree.nodes(bTree.root);
        var expected = 6;
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree toArray', () => {
    var bTree = new BSTree();
    test('BSTree nodes is', () => {
        bTree.init([8,3,12,2,4,9,15]);
        var action = bTree.toArray();
        var expected = [8, 3, 2, 4, 12, 9, 15];
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree remove', () => {
    var bTree = new BSTree();
    test('BSTree remove is', () => {
        bTree.init([8,3,12,2,4,9,15]);
        var action = bTree.remove(4);
        var expected = {
            "left": {
                "left": {
                    "left": null,
                    "right": null,
                    "value": 2
                },
                "right": null,
                "value": 3
            },
            "right": {
                "left": {
                    "left": null,
                    "right": null,
                    "value": 9
                },
                "right": {
                    "left": null,
                    "right": null,
                    "value": 15
                },
                "value": 12
            },
            "value": 8
        };
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree print', () => {
    var bTree = new BSTree();
    test('BSTree print is', () => {
        bTree.init([8,3,12,2,4,9,15]);
        var action = bTree.toArray();
        var expected = [8, 3, 2, 4, 12, 9, 15];
        expect(action).toEqual(expected);
        bTree.clear();
    });
});

describe('BSTree width', () => {
    var bTree = new BSTree();
    test('BSTree width is', () => {
        bTree.init([8,3,12,2,4,9,15]);
        var action = bTree.getWidth();
        var expected = 4;
        expect(action).toEqual(expected);
        bTree.clear();
    });
});


