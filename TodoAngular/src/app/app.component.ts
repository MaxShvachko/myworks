import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})

export class AppComponent {
  title = 'My todos';
  todos = [
    {label: 'Bring Milk', done: false, priority: 3},
    {label: 'Bring pickles', done: true, priority: 4},
    {label: 'Bring Sigarets', done: false, priority: 1},
    {label: 'Bring paper', done: false, priority: 3},
    ];

  addTodo(newTodoLabel) {
    let newTodo = {
      label: newTodoLabel,
      priority: 1,
      done: false,
    };
    this.todos.push(newTodo);
  }

  toggleDoneTodo(todo) {
    const currentTodo = this.todos.findIndex((element, i ) => element.label === todo.label);
    this.todos[currentTodo].done = !this.todos[currentTodo].done;
  }

  deleteTodo(todo) {
    this.todos = this.todos.filter(item => todo.label !==  item.label);
  }
}
