export class Todo {
  id: number;
  title: string = '';
  complete: boolean = false;

  constructor (value: Object = {}) {
    Object.assign(this, value);
  }
}

let todo = new Todo({
  title: 'Read SitePoint article',
  complete: false,

});


