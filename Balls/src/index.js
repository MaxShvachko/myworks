import "./style/index.less"

let arrayBalls = [];
let interval = undefined;

const canvas = document.getElementById("main-canvas");
const ctx = canvas.getContext('2d');
const start = document.getElementById('start');
const clear = document.getElementById('clear');

start.addEventListener('click', () => {startGame(); toggleStartDisabled()} );
clear.addEventListener('click', () => {stop(interval); toggleClearDisabled()});

// toggle class start and clear
function toggleStartDisabled() {
  start.setAttribute('disabled', 'true');
  clear.removeAttribute('disabled');
}

function toggleClearDisabled() {
  clear.setAttribute('disabled', 'true');
  start.removeAttribute('disabled');

}

function startGame() {
  interval = setInterval(() => {
    movement(arrayBalls)
  }, 10);
  return interval
}

function stop() {
  clearInterval(interval);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  arrayBalls = [];
  interval = undefined;
}

// random circle
function randomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function randomMovement() {
  return Math.floor(Math.random() * (5 - -5)) + (-5);
}

function randomRadius() {
  return Math.floor(Math.random() * (55 - -6)) + 5;
}

// View constructor
function View() {
  // this.balls = [];
  this.canvas = document.getElementById("main-canvas");
  this.ctx = this.canvas.getContext('2d');
  this.canvas.addEventListener('mousedown', this.onMouseDown.bind(this));
}

View.prototype.onMouseDown = function(e) {
  if (isNaN(interval)) {
    return
  }
  const {offsetX, offsetY} = e;
  let x = offsetX;
  let y = offsetY;

  const radius = randomRadius();
  if (x - radius <= this.canvas.width) {
    x = x + radius;
  }
  if (x + radius >= this.canvas.width) {
    x = x - radius * 2;
  }
  if (y - radius <= this.canvas.height) {
    y = y + radius;
  }
  if (y + radius >= this.canvas.height) {
    y = y - radius * 2;
  }

  const ball = new Ball(x, y, radius);
  arrayBalls.push(ball);
};

function drawBall (ball) {
  ctx.beginPath();
  ctx.arc(ball.x, ball.y, ball.radius, 0, 2 * Math.PI);
  ctx.fillStyle = ball.color;
  ctx.closePath();
  ctx.fill();
}

function movement (balls) {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  balls.forEach(ball => {
    if(ball.x + ball.speedX > canvas.width - ball.radius || ball.x + ball.speedX < ball.radius) {
      ball.speedX = -ball.speedX;
    }
    if(ball.y + ball.speedY > canvas.height - ball.radius || ball.y + ball.speedY < ball.radius) {
      ball.speedY = -ball.speedY;
    }
    ball.x = ball.x + ball.speedX;
    ball.y = ball.y + ball.speedY;
    drawBall(ball);
  })
}

// Ball constructor
function Ball(x, y, radius) {
  this.x = x;
  this.y = y;
  this.radius = radius;
  this.color = randomColor();
  this.speedX = randomMovement();
  this.speedY = randomMovement();

}
// init of app
const init = () => {
  const view = new View();
};

document.addEventListener("DOMContentLoaded",  init);